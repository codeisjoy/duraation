//
//  SQLiteTests.swift
//  Duraation
//
//  Created by Emad A. on 13/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit
import XCTest

class SQLiteTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    /*
    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    */

    func testInsertActivity() {
        let a_1 = Activity()
        a_1.start = NSDate(timeIntervalSinceNow: 0)
        a_1.end = a_1.start.dateByAddingTimeInterval(60 * 60)
        a_1.note = "#Hello from here no #one"
        DataCenter.defaultCenter().save(a_1, completion: nil)

        let a_2 = Activity()
        a_2.start = NSDate(timeIntervalSinceNow: 60 * 60 * 20)
        a_2.end = a_2.start.dateByAddingTimeInterval(60 * 60)
        a_2.note = "#Hello from here no #two"
        DataCenter.defaultCenter().save(a_2, completion: nil)
    }

    func testDeleteHashtag() {
        let activity = Activity()
        activity.uid = 1

        DataCenter.defaultCenter().delete(activity, completion: nil)
    }


    func testSelectAllActivities() {
        let activities = DataCenter.defaultCenter().getActivities(forDate: NSDate(timeIntervalSinceNow: 60 * 60 * 40))
        println(activities)
    }

    func testInsertOverview() {
        let overview_1 = Overview()
        overview_1.dateRangeType = OverviewDateRange.Last7Days
        overview_1.hashtags = ["a","b","c"]

        let overview_2 = Overview()
        overview_2.dateRangeType = OverviewDateRange.Custom
        overview_2.customDateRange = (from: NSDate(timeIntervalSinceNow: 60 * 60 * 24 * 7), to: NSDate())
        overview_2.hashtags = ["d","e"]

        DataCenter.defaultCenter().save(overview_1, completion: nil)
        DataCenter.defaultCenter().save(overview_2, completion: nil)
    }

    func testSelectAllOverviews() {
        let overviews = DataCenter.defaultCenter().getOverviews()
        println(overviews)
    }

    func testDurationOfOverview() {
        let overview = Overview()
        overview.hashtags = ["wtf4","abc","z", "lOnG"];
        overview.dateRangeType = .Last60Days
        overview.title = "Test"
        overview.uid = 1

//        DataCenter.defaultCenter().getActivitiesForOverview(overview)
    }

}
