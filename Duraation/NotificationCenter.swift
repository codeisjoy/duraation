//
//  NotificationCenter.swift
//  Duraation
//
//  Created by Emad A. on 12/04/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

// MARK: NoticeMessage
// MARK:

enum NoticeMessage: Int {
    case
    // activity.start > activity.end
    ErrorActivityTimeInvalid = 1001,
    // activity.note.length == 0
    ErrorActivityNoteEmpty,
    // activity.note.length > maxlength
    ErrorActivityNoteTooLong,
    // overview.title.length == 0
    ErrorOverviewTitleEmpty,
    // overview.title.length > maxLength
    ErrorOverviewTitleTooLong,
    // overview.dateRange == none
    ErrorOverviewDateRangeEmpty,
    // overview.customDateRange.from > overview.customDateRange.to
    ErrorOverviewCustomDateRangeInvalid,
    // overview.hashtags.count == 0
    ErrorOverviewHashtagsEmpty,
    // booooo!
    ErrorItemDidNotSave,


    // note.hashtags.count == 0
    WarningActivityHashtagEmptyDidSave = 2001,

    // thumbs up!
    SuccessItemDidSave = 4001

    func toString() -> String {
        switch self {
        case .ErrorActivityTimeInvalid:
            return "Ha? How do you finish a job before starting it? Enter the time carefully."
        case .ErrorActivityNoteEmpty:
            return [
                "Woe! If you did nothing in this time why do you insist to save it?",
                "Well! I bet you've done something in this time but forgot to enter that."
                ].sample()
        case .ErrorActivityNoteTooLong:
            return [
                "Sigh! Your note is too long. Make it a little shorter than a tweet.",
                "Humm! What an extensive note, but it is not acceptable. Shorten it."
                ].sample()
        case .ErrorOverviewTitleEmpty:
            return "Ah! Put a name on this overview."
        case .ErrorOverviewTitleTooLong:
            return "Humm! The title is too long. Shorten it a little."
        case .ErrorOverviewDateRangeEmpty:
            return "Sorry! In what date range you mean?"
        case .ErrorOverviewCustomDateRangeInvalid:
            return "Seriously! Why do you think 'from' can be after 'to'?"
        case .ErrorOverviewHashtagsEmpty:
            return "Oh! What hashtags are you searching for, then?"
        case .ErrorItemDidNotSave:
            return "Ah! Something bad happend. Not able to save the item."

        case .WarningActivityHashtagEmptyDidSave:
            return "OK! Activity saved but no #hashtag in there. That's not searchable."

        case .SuccessItemDidSave:
            return [
                "Sweet! You have just saved it.",
                "Brilliant! That is safe there.",
                "Awesome! Another one has just been added.",
                "Wonderful! That has been saved."
                ].sample()

        default:
            return ""
        }
    }
}

// MARK: - Notice
// MARK:

typealias NoticeType = EMNoticeType

struct Notice {

    let duration: NSTimeInterval = 5
    var type: NoticeType
    var message: String
    var image: UIImage?

    init(message: NoticeMessage) {
        self.message = message.toString()
        if message.rawValue > 1000 && message.rawValue < 2000 {
            self.type = NoticeType.Error
        }
        else if message.rawValue > 2000 && message.rawValue < 3000 {
            self.type = NoticeType.Warning
        }
        else if message.rawValue > 3000 && message.rawValue < 4000 {
            self.type = NoticeType.Info
        }
        else {
            self.type = NoticeType.Success
        }

        var imageName: String
        switch type {
        case NoticeType.Error:   imageName = "error"
        case NoticeType.Warning: imageName = "warning"
        case NoticeType.Info:    imageName = "info"
        case NoticeType.Success: imageName = "success"
        }
        image = UIImage(named: imageName)
    }
}

// MARK: - NotificationCenter
// MARK:

class NotificationCenter: EMNoticeCenter {

    override class func defaultCenter() -> NotificationCenter {
        struct Static {
            static let instance: NotificationCenter = NotificationCenter()
        }
        return Static.instance
    }

    func fireNotice(notice: Notice) {
        fireNotice(
            duration: notice.duration,
            type: notice.type,
            message: notice.message,
            image: notice.image)
    }
}