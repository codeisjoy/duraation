//
//  DatePickerViewController.swift
//  Duraation
//
//  Created by Emad A. on 14/02/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {

    // MARK: - Public Properties

    @IBOutlet weak var navView: ModalNavigationView?
    @IBOutlet weak var datePickerView: DatePickerView?

    // MARK: Private Properties

    private var doneAction: ((DatePickerViewController, NSDate) -> Void)?

    // MARK: - Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        navView?.doneButtonHidden = false
        navView?.titleLabel.text = "Date Picker"
        navView?.doneButton.addTarget(self, action: Selector("done"), forControlEvents: .TouchUpInside)

        datePickerView?.style = .Date
    }

    // MARK: - Public Methods

    func setDoneAction(action: (DatePickerViewController, NSDate) -> Void) {
        doneAction = action
    }

    func dismiss(#completion: (Void -> Void)?) {
        parentViewController?.dismissViewControllerAnimated(true, completion: completion)
    }

    // MARK: - Private Methods

    @objc private func done() {
        doneAction?(self, datePickerView!.pickedDate)
    }
}