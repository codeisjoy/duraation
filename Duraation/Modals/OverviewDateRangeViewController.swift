//
//  OverviewDateRangeViewController.swift
//  Duraation
//
//  Created by Emad A. on 9/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

// MARK: - OverviewDateRangeViewController
// MARK: -

class OverviewDateRangeViewController: UIViewController {

    private let tableViewCellId = "DateRangeCell"

    // MARK: - Public Methods

    @IBOutlet weak var navView: ModalNavigationView?
    @IBOutlet weak var tableView: UITableView?

    weak var overview: Overview?

    // MARK: - Private Methods

    private var cells = [OverviewDateRange]()

    // Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        cells = [.All, .Last60Days, .Last28Days, .Last7Days, .Custom]

        // Setting navigation view
        navView?.backButtonHidden = false
        navView?.titleLabel.text = "Date range"
        navView?.backButton.addTarget(self, action: Selector("backToRootViewController"), forControlEvents: .TouchUpInside)

        // Setting table view
        tableView?.tableFooterView = UIView()
        tableView?.layoutMargins = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "custom" {
            let destination = segue.destinationViewController as! OverviewCustomDateRangeViewController
            destination.overview = overview
        }
    }

    // MARK: - Private Methods

    func backToRootViewController() {
        navigationController?.popViewControllerAnimated(true)
    }

}

// MARK: - UITableViewDataSource Methods
// MARK: -

extension OverviewDateRangeViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        (cell as! EntryFormSelectionalCell).optionSelected = (cells[indexPath.row] == overview?.dateRangeType)
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(tableViewCellId) as? EntryFormSelectionalCell
        if cell == nil {
            cell = EntryFormSelectionalCell(style: .Default, reuseIdentifier: tableViewCellId)
            cell!.textLabel?.textColor = UIColor.mineShaftColor()
            cell!.textLabel?.font = ApplicationFontStyle.MainTitle.toFont()
            cell!.layoutMargins = tableView.layoutMargins
        }

        cell!.textLabel?.text = cells[indexPath.row].toString()

        return cell!
    }
}

// MARK: - UITableViewDelegate Methods
// MARK: -

extension OverviewDateRangeViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 54
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Turn option selected on/off for all cells
        // The previous selection should be off and new one on
        for var index = 0; index < cells.count; ++index {
            if let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? EntryFormSelectionalCell {
                cell.optionSelected = indexPath.row == index
            }
        }

        // Show custom date range selection view controller
        if indexPath.row == find(cells, .Custom) {
            performSegueWithIdentifier("custom", sender: self)
        }
        // Pop to form main view controller
        else {
            overview?.dateRangeType = cells[indexPath.row]
            overview?.customDateRange = nil
            backToRootViewController()
        }
    }
}
