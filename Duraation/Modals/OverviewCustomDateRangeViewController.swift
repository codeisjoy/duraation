//
//  OverviewCustomDateRangeViewController.swift
//  Duraation
//
//  Created by Emad A. on 9/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class OverviewCustomDateRangeViewController: UIViewController {

    // MARK: - Public Properties

    @IBOutlet weak var navView: ModalNavigationView?
    @IBOutlet weak var tableView: UITableView?

    weak var overview: Overview?

    // MARK: - Private Properties

    private var cells = Array<EntryFormDatePickerCell>()

    private lazy var fromCell: EntryFormDatePickerCell = {
        let cell = EntryFormDatePickerCell(caption: "FROM")
        cell.datePicker.style = DatePickerStyle.Date
        return cell
        }()

    // The cell to enter end date
    private lazy var toCell: EntryFormDatePickerCell = {
        let cell = EntryFormDatePickerCell(caption: "TO")
        cell.datePicker.style = DatePickerStyle.Date
        return cell
        }()

    // MARK: - Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        cells = [fromCell, toCell]

        // Setting navigation view
        navView?.backButtonHidden = false
        navView?.doneButtonHidden = false
        navView?.titleLabel.text = "Custom date range"
        navView?.backButton.addTarget(
            self,
            action: Selector("backToDateRangeViewController"),
            forControlEvents: .TouchUpInside)
        navView?.doneButton.addTarget(
            self,
            action: Selector("backToRootViewController"),
            forControlEvents: .TouchUpInside)

        // Setting table view
        tableView?.tableFooterView = UIView()
        tableView?.layoutMargins = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if let dateRange = overview?.customDateRange where dateRange.from != nil && dateRange.to != nil {
            fromCell.datePicker.pickedDate = dateRange.from!
            toCell.datePicker.pickedDate = dateRange.to!
        }
        else {
            let defaultDate = NSDate()
            let calendar = NSCalendar.currentCalendar()

            // Set the first day of current month as 'From' value
            let fromComponents = calendar.components(.CalendarUnitYear | .CalendarUnitMonth, fromDate: defaultDate)
            fromComponents.day = 1
            fromCell.datePicker.pickedDate =
                calendar.dateFromComponents(fromComponents) ??
                defaultDate.dateByAddingTimeInterval(60 * 60 * 24 * 7 * -1)

            // Set the last day of current month as 'To' value
            let toComponents = calendar.components(.CalendarUnitYear | .CalendarUnitMonth, fromDate: defaultDate)
            toComponents.day = 0
            toComponents.month += 1
            toCell.datePicker.pickedDate =
                calendar.dateFromComponents(toComponents) ??
                defaultDate

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Private Methods

    func backToRootViewController() {
        if validatePickedDates() == false {
            NotificationCenter.defaultCenter().fireNotice(Notice(message: .ErrorOverviewCustomDateRangeInvalid))
            return
        }

        let calendar = NSCalendar.currentCalendar()
        let to = calendar.components(.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay, fromDate: toCell.datePicker.pickedDate)
        to.second = -1
        to.day += 1

        overview?.customDateRange = (
            from: fromCell.datePicker.pickedDate,
            to: calendar.dateFromComponents(to))
        overview?.dateRangeType = .Custom

        navigationController?.popToRootViewControllerAnimated(true)
    }

    func backToDateRangeViewController() {
        validatePickedDates()
        navigationController?.popViewControllerAnimated(true)
    }

    func validatePickedDates() -> Bool {
        let from = fromCell.datePicker.pickedDate
        let to = toCell.datePicker.pickedDate

        return from.compare(to) != NSComparisonResult.OrderedDescending
    }

}

// MARK: - UITableViewDataSource Methods
// MARK: -

extension OverviewCustomDateRangeViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell = cells[indexPath.row]
        cell.layoutMargins = tableView.layoutMargins

        return cell
    }

}

// MARK: - UITableViewDelegate Methods
// MARK: -

extension OverviewCustomDateRangeViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return cells[indexPath.row].preferredHeight
    }

    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        // Closing the cell if it is open now
        if tableView.cellForRowAtIndexPath(indexPath)?.selected == true {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            tableView.beginUpdates()
            tableView.endUpdates()

            return nil
        }

        return indexPath
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.beginUpdates()
        tableView.endUpdates()
    }

}
