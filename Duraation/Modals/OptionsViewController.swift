//
//  ActivityOptionsViewController.swift
//  Duraation
//
//  Created by Emad A. on 4/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController {

    // MARK: - Constants

    // Where round corners are needed
    private let cornerRadius: CGFloat = 5
    // Default value of buttons height
    private let buttonHeight: CGFloat = 50
    // Content padding
    private let contentEdgeInsets: UIEdgeInsets = UIEdgeInsets(top: 4, left: 10, bottom: 10, right: 10)

    // MARK: - Public Properties

    var userInfo: AnyObject?

    // The best height in which all content is fit
    var prefferedHeight: CGFloat {
        let optionButtonsHeight =
            CGFloat(buttonsContainer.subviews.count) * buttonHeight +
            max(CGFloat(buttonsContainer.subviews.count - 1), 0) * (1 / UIScreen.mainScreen().scale)
        return optionButtonsHeight + buttonHeight + contentEdgeInsets.bottom * 2 + contentEdgeInsets.top
    }

    // MARK: - Private Properties

    // Option buttons superview
    private lazy var buttonsContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = self.cornerRadius
        view.backgroundColor = UIColor(white: 1, alpha: 0.75)
        view.clipsToBounds = true
        return view
        }()

    // The button to dismiss the view controller
    private lazy var cancelButton: OptionButtonItem = {
        let cancel = OptionButtonItem(title: "Cancel") { [weak self] in
            self?.dismiss(completion: nil)
        }
        cancel.titleLabel.font = ApplicationFontStyle.MainTitle.toFont()
        cancel.titleLabel.textColor = UIColor.mineShaftColor()
        cancel.layer.cornerRadius = self.cornerRadius
        return cancel
        }()

    // Keeps the target and action that should be called on cancel
    private var cancelCompletion: (Void -> Void)?

    // MARK: Overriden Methods

    convenience init(optionButtons: [OptionButtonItem], cancelCompletion: (Void -> Void)?) {
        self.init()

        for button in optionButtons {
            buttonsContainer.addSubview(button)
        }
        self.cancelCompletion = cancelCompletion
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.clipsToBounds = true
        view.backgroundColor = UIColor.clearColor()

        view.addSubview(buttonsContainer)
        view.addSubview(cancelButton)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        // Positioning the cancel button at the bottom of the view
        let cancelButtonSize = CGSize(
            width: view.bounds.width - contentEdgeInsets.left - contentEdgeInsets.right,
            height: buttonHeight)
        cancelButton.frame = CGRect(
            origin: CGPoint(x: contentEdgeInsets.left, y: CGRectGetMaxY(view.bounds) - cancelButtonSize.height - contentEdgeInsets.bottom),
            size: cancelButtonSize)

        // Positioning the options button container above the cancel button
        let containerHeight = CGFloat(buttonsContainer.subviews.count) * buttonHeight +
            max(CGFloat(buttonsContainer.subviews.count) - 1, 0) * (1 / UIScreen.mainScreen().scale)
        buttonsContainer.frame = CGRect(
            origin: CGPoint(x: contentEdgeInsets.left, y: CGRectGetMinY(cancelButton.frame) - containerHeight - contentEdgeInsets.bottom),
            size: CGSize(width: view.bounds.width - contentEdgeInsets.left - contentEdgeInsets.right, height: containerHeight))

        // Positioning the option buttons according to the their index in container view
        for var index = 0; index < buttonsContainer.subviews.count; ++index {
            let button = buttonsContainer.subviews[index] as! OptionButtonItem
            let y = CGFloat(index) * buttonHeight + CGFloat(index) * (1 / UIScreen.mainScreen().scale)
            button.frame = CGRect(
                origin: CGPoint(x: CGRectGetMinX(buttonsContainer.bounds), y: y ),
                size: CGSize(width: CGRectGetWidth(buttonsContainer.bounds), height: buttonHeight))
        }
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        cancelCompletion?()
    }

    // MARK: - Public Methods

    func dismiss(#completion: (Void -> Void)?) {
        parentViewController?.dismissViewControllerAnimated(true, completion: completion)
    }

}

// MARK: - OptionButtonItem
// MARK: -

class OptionButtonItem: UIControl {

    // Number of seconds in which confirming can take place
    private let confirmationSec: CGFloat = 3
    // Default confirmation message format
    private let confirmationMsg: String = "Double tap to confirm in %d sec."

    // MARK: - Public Properties

    // Determin if the button needs confirmation to call the action
    var needConfirmation: Bool = false

    // Mark: - Private Properties

    // The action of the button
    private var action: (Void -> Void)?

    // Timer to count down confirmatoin duration
    private var confirmationTimer: NSTimer?
    // Keeps active confirmation seconds are going on
    private var activeConfirmationSec: CGFloat = 0

    // The title label of the button
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .Center
        label.backgroundColor = UIColor.clearColor()
        return label
        }()

    // The confirmation label of the button
    private lazy var confirmationLabel: UILabel = {
        let label = UILabel()
        label.alpha = 0.8
        label.textAlignment = .Center
        label.userInteractionEnabled = true
        label.backgroundColor = UIColor.clearColor()
        // Defining double tap gesture recognizer for confirmation label
        let dbltap = UITapGestureRecognizer(target: self, action: Selector("handleDoubleTap:"))
        dbltap.numberOfTapsRequired = 2
        label.addGestureRecognizer(dbltap)

        return label
        }()

    // MARK: - Overriden Methods

    convenience init(title: String?, needConfirmation: Bool = false, action: (Void -> Void)? = nil) {
        self.init()

        self.needConfirmation = needConfirmation
        self.action = action

        backgroundColor = UIColor.whiteColor()

        titleLabel.text = title
        addSubview(titleLabel)

        // Default action should be done on button selection
        addTarget(self, action: Selector("didSelect"), forControlEvents: .TouchUpInside)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        // Confirmation mode is not active
        if selected == false {
            titleLabel.frame = bounds
        }
        // In confirmation mode
        else {
            var frame = bounds
            frame.origin.y = CGRectGetMinY(bounds) - CGRectGetHeight(frame)
            titleLabel.frame = frame

            confirmationLabel.frame = CGRect(
                origin: CGPoint(x: CGRectGetMinX(bounds), y: CGRectGetMaxY(bounds)),
                size: bounds.size)
        }
    }

    // MARK: - Private Methods

    @objc private func didSelect() {
        // Do nothing if confirmation mode is active
        if selected == true {
            return
        }

        if needConfirmation == true {
            selected = true
            showConformationView()
        }
        else {
            action?()
        }
    }

    @objc private func handleDoubleTap(gestureRecognizer: UITapGestureRecognizer) {
        action?()
    }

    private func showConformationView() {
        // Setting the confirmation label default properties before being shown
        confirmationLabel.textColor = titleLabel.textColor
        confirmationLabel.text = String(format: confirmationMsg, arguments: [Int(confirmationSec)])
        confirmationLabel.font = UIFont(name: titleLabel.font.fontName, size: max(titleLabel.font.pointSize - 3, 12))
        // After adding the confirmation label into view layout is needed.
        addSubview(confirmationLabel)
        layoutIfNeeded()

        UIView.animateWithDuration(
            0.25,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity:  1,
            options: UIViewAnimationOptions.allZeros,
            animations: {
                // Sliding title label up and hiding it
                var frame = self.titleLabel.frame
                frame.origin.y = CGRectGetMinY(self.bounds) - CGRectGetHeight(frame)
                self.titleLabel.frame = frame
                // Sliding confirmation label up and showing it
                frame = self.confirmationLabel.frame
                frame.origin.y = CGRectGetMinY(self.bounds)
                self.confirmationLabel.frame = frame
            },
            completion: { (finished) -> Void in
                // Firing the timer to count seconds
                self.activeConfirmationSec = 0
                self.confirmationTimer = NSTimer.scheduledTimerWithTimeInterval(
                    1,
                    target: self,
                    selector: Selector("countConfirmationSecDown"),
                    userInfo: nil,
                    repeats: true)
            })
    }

    private func dismissConfirmationView() {
        UIView.animateWithDuration(
            0.25,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity:  1,
            options: UIViewAnimationOptions.allZeros,
            animations: {
                // Sliding title label down and showing it
                var frame = self.titleLabel.frame
                frame.origin.y = CGRectGetMinY(self.bounds)
                self.titleLabel.frame = frame
                // Sliding confirmation label down and hiding it
                frame = self.confirmationLabel.frame
                frame.origin.y = CGRectGetMaxY(self.bounds)
                self.confirmationLabel.frame = frame
            },
            completion: { (finished) -> Void in
                self.selected = false
                self.confirmationLabel.removeFromSuperview()
            })
    }

    @objc private func countConfirmationSecDown() {
        activeConfirmationSec += 1
        if activeConfirmationSec >= confirmationSec {
            confirmationTimer?.invalidate()
            dismissConfirmationView()

            return
        }

        confirmationLabel.text = String(format: confirmationMsg, arguments: [Int(confirmationSec - activeConfirmationSec)])
    }
}