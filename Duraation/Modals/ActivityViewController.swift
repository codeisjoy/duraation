//
//  ActivityViewController.swift
//  Duraation
//
//  Created by Emad A. on 25/02/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController {

    // MARK: - Public Properties

    @IBOutlet weak var navView: ModalNavigationView?
    @IBOutlet weak var tableView: UITableView?

    var activity: Activity = Activity() {
        didSet { propagateActivityChangeOnView() }
    }

    // MARK: - Private Properties

    // The cell to enter start date
    private lazy var startCell: EntryFormDatePickerCell = {
        let cell = EntryFormDatePickerCell(caption: "START")
        return cell
        }()

    // The cell to enter end date
    private lazy var endCell: EntryFormDatePickerCell = {
        let cell = EntryFormDatePickerCell(caption: "END")
        return cell
        }()

    private lazy var noteCell: EntryFormTextViewCell = {
        let cell = EntryFormTextViewCell(caption: "Note ...")
        cell.textView.keyboardType = UIKeyboardType.Twitter
        cell.textView.returnKeyType = UIReturnKeyType.Done
        cell.textView.delegate = self
        return cell
        }()

    private var doneAction: ((ActivityViewController, Activity) -> Void)?

    // MARK: - Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Configuring navigation bar
        navView?.doneButtonHidden = false
        navView?.titleLabel.text = "An Activity"
        navView?.doneButton.addTarget(self, action: Selector("done"), forControlEvents: .TouchUpInside)
        
        tableView?.layoutMargins = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)
        tableView?.tableFooterView = UIView()

        // Setting lestening to keyboard frame change
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: Selector("keyboardFrameWillChange:"),
            name: UIKeyboardWillChangeFrameNotification,
            object: nil)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        // To set activity values in related view element
        propagateActivityChangeOnView()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // To resign the first responder and other resources
        if let selected = tableView?.indexPathForSelectedRow() {
            tableView?.deselectRowAtIndexPath(selected, animated: false)
        }
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    // MARK: - Public Methods

    func setDoneAction(action: (ActivityViewController, Activity) -> Void) {
        doneAction = action
    }

    func dismiss(#completion: (Void -> Void)?) {
        parentViewController?.dismissViewControllerAnimated(true, completion: completion)
    }

    // MARK: - Private Methods

    @objc private func keyboardFrameWillChange(notice: NSNotification) {
        if let
            window = view.window,
            userInfo = notice.userInfo,
            endRectValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue
        {
            let edgeInsetBottom: CGFloat = window.bounds.height - endRectValue.CGRectValue().origin.y
            tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: edgeInsetBottom, right: 0)
            tableView?.beginUpdates()
            tableView?.endUpdates()

            tableView?.scrollToNearestSelectedRowAtScrollPosition(.Top, animated: true)
        }
    }

    @objc private func done() {
        // Resign the first responder and other resources.
        if let selected = tableView?.indexPathForSelectedRow() {
            tableView?.deselectRowAtIndexPath(selected, animated: false)
        }

        // Create a model object with current form values.
        let model: Activity = activity ?? Activity()
        model.start = startCell.datePicker.pickedDate
        model.end = endCell.datePicker.pickedDate
        model.note = noteCell.textView.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        model.hashtags = HashtagHelper.hashtagsInText(model.note)

        // Call the done action has been set by parent and pass model object.
        doneAction?(self, model)
    }

    // MARK: - Private Methods

    private func propagateActivityChangeOnView() {
        startCell.datePicker.pickedDate = activity.start
        endCell.datePicker.pickedDate = activity.end

        noteCell.textView.text = activity.note
        noteCell.captionLabel.hidden = count(noteCell.textView.text) > 0
    }
}

// MARK: - UITableViewDataSource Methods
// MARK: -

extension ActivityViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        switch indexPath.row {
        case 0: cell = startCell
        case 1: cell = endCell
        case 2: cell = noteCell
        default:
            cell = UITableViewCell()
        }
        cell.layoutMargins = tableView.layoutMargins

        return cell
    }
}

// MARK: - UITableViewDelegate Methods
// MARK: -

extension ActivityViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height: CGFloat = 0
        if indexPath.row == 0 {
            height = startCell.preferredHeight
        }
        else if indexPath.row == 1 {
            height = endCell.preferredHeight
        }
        else if indexPath.row == 2 {
            // There is a selected cell in table view.
            if let selected = tableView.indexPathForSelectedRow() {
                // If note cell is selected, should fill the room above keyboard and
                // if is not, the cell will decide what should be the height.
                height = selected.row == 2 ? tableView.bounds.height - tableView.contentInset.bottom : noteCell.preferredHeight
            }
            // There is no selection in table view.
            else {
                // The note cell should stretch to the end of visible view.
                height = tableView.bounds.height - startCell.preferredHeight - endCell.preferredHeight
            }
        }

        return height
    }

    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        // If start or end cell are going to be selected again
        // in fact they should be selected and closed.
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        if cell?.selected == true && (cell == startCell || cell == endCell) {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            tableView.beginUpdates()
            tableView.endUpdates()

            return nil
        }

        return indexPath
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.beginUpdates()
        tableView.endUpdates()

        tableView.scrollToNearestSelectedRowAtScrollPosition(.Top, animated: true)
    }
}

// MARK: - UITextViewDelegate
// MARK: -

extension ActivityViewController: UITextViewDelegate {

    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        // Resign the text view of being the first responder if return key pressed.
        if text == "\n" && textView.canResignFirstResponder() {
            tableView?.deselectRowAtIndexPath(NSIndexPath(forRow: 2, inSection: 0), animated: true)
            tableView?.beginUpdates()
            tableView?.endUpdates()

            return false
        }

        return true
    }
}