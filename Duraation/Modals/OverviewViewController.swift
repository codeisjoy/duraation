//
//  OverviewViewController.swift
//  Duraation
//
//  Created by Emad A. on 8/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class OverviewViewController: UIViewController {
    
    // MARK: - Public Properties

    @IBOutlet weak var navView: ModalNavigationView?
    @IBOutlet weak var tableView: UITableView?

    var overview = Overview() {
        didSet { propagateOverviewChangeOnView() }
    }

    // MARK: - Private Properties

    private var cells = Array<EntryFormBasicCell>()

    private lazy var persistentCell: EntryFormSelectionalCell = {
        let cell = EntryFormSelectionalCell()
        cell.optionSelected = true
        cell.textLabel?.text = "PERSISTENT"
        cell.textLabel?.textColor = UIColor.aluminumColor()
        cell.textLabel?.font = ApplicationFontStyle.TeenyTiny.toFont()
        return cell
        }()

    private lazy var titleCell: EntryFormTextFieldCell = {
        let cell = EntryFormTextFieldCell()
        cell.captionLabel.text = "TITLE"
        cell.textField.placeholder = "Not set yet"
        cell.textField.delegate = self
        cell.textField.returnKeyType = .Done
        cell.textField.clearButtonMode = .WhileEditing
        return cell
        }()

    private lazy var dateRangeCell: EntryFormLabelledCell = {
        let cell = EntryFormLabelledCell()
        cell.captionLabel.text = "DATE RANGE"
        cell.valueLabel.placeholder = "Not set yet"
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
        }()

    private lazy var hashtagsCell: EntryFormLabelledCell = {
        let cell = EntryFormLabelledCell()
        cell.captionLabel.text = "HASHTAGS"
        cell.valueLabel.placeholder = "Not set yet"
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
        }()

    private var doneAction: ((OverviewViewController, Overview, Bool) -> Void)?

    // MARK: - Overriten Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // table view cells in order
        cells = [persistentCell, titleCell, dateRangeCell, hashtagsCell]

        // Setting navigation view
        navView?.doneButtonHidden = false
        navView?.titleLabel.text = "An Overview"
        navView?.doneButton.addTarget(self, action: Selector("done"), forControlEvents: .TouchUpInside)

        // Setting table view
        tableView?.tableFooterView = UIView()
        tableView?.layoutMargins = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)

        // Setting listening to keyboard frame change
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: Selector("keyboardFrameWillChange:"),
            name: UIKeyboardWillChangeFrameNotification,
            object: nil)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        // To set overview values in related view element
        propagateOverviewChangeOnView()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        deselectSelectedCell()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        overview.title = titleCell.textField.text
        if segue.identifier == "daterange" {
            let destination = segue.destinationViewController as! OverviewDateRangeViewController
            destination.overview = overview
        }
        else if segue.identifier == "hashtags" {
            let destination = segue.destinationViewController as! OverviewHashtagsViewController
            destination.overview = overview
        }
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    // MARK: - Public Method

    func setDoneAction(action: (OverviewViewController, Overview, Bool) -> Void) {
        doneAction = action
    }

    func dismiss(#completion: (Void -> Void)?) {
        parentViewController?.dismissViewControllerAnimated(true, completion: completion)
    }

    // MARK: - Private Methods

    private func propagateOverviewChangeOnView() {
        titleCell.textField.text = overview.title

        // Show selected date range in related field, if any.
        if overview.dateRangeType == OverviewDateRange.Custom {
            if let dateRange = overview.customDateRange where dateRange.from != nil && dateRange.to != nil {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM yyyy"
                var dateRangeString = dateFormatter.stringFromDate(dateRange.from!)
                let dateRangeStringTo = dateFormatter.stringFromDate(dateRange.to!)
                if (dateRangeString != dateRangeStringTo) {
                    dateRangeString += " - " + dateRangeStringTo
                }
                dateRangeCell.valueLabel.text = dateRangeString
            }
        }
        else {
            dateRangeCell.valueLabel.text = overview.dateRangeType.toString()
        }

        // Show selected hahstags in related field, if any.
        var hashtagsValue = ""
        if overview.hashtags.isEmpty == false {
            hashtagsValue = "#" + overview.hashtags.last!
            if overview.hashtags.count > 1 {
                hashtagsValue += " and \(overview.hashtags.count - 1) more"
            }
        }
        hashtagsCell.valueLabel.text = hashtagsValue
    }

    @objc private func keyboardFrameWillChange(notice: NSNotification) {
        if let
            window = view.window,
            userInfo = notice.userInfo,
            endRectValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue
        {
            let edgeInsetBottom: CGFloat = window.bounds.height - endRectValue.CGRectValue().origin.y
            tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: edgeInsetBottom, right: 0)
            tableView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: edgeInsetBottom, right: 0)
            
            tableView?.scrollToNearestSelectedRowAtScrollPosition(.Top, animated: true)
        }
    }

    private func deselectSelectedCell() {
        if let selected = tableView?.indexPathForSelectedRow() {
            tableView?.deselectRowAtIndexPath(selected, animated: false)
            tableView?.delegate?.tableView!(tableView!, didDeselectRowAtIndexPath: selected)
        }
    }

    func done() {
        // Resign the first responder and other resources.
        if let selected = tableView?.indexPathForSelectedRow() {
            tableView?.deselectRowAtIndexPath(selected, animated: false)
        }

        overview.title = titleCell.textField.text
        doneAction?(self, overview, persistentCell.optionSelected)
    }
    
}

// MARK: - UITableViewDataSource Methods
// MARK: -

extension OverviewViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell = cells[indexPath.row]
        cell.layoutMargins = tableView.layoutMargins
        
        return cell
    }

}

// MARK: - UITableViewDelegate Methods
// MARK: -

extension OverviewViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return cells[indexPath.row].preferredHeight
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Show date range view controller
        if indexPath.row == find(cells, dateRangeCell) {
            performSegueWithIdentifier("daterange", sender: self)
        }
        else if indexPath.row == find(cells, hashtagsCell) {
            performSegueWithIdentifier("hashtags", sender: self)
        }
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == find(cells, titleCell) {
//            overview.title = titleCell.textField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        }
    }

}

// MARK: - UITextFieldDelegate Methods
// MARK: -

extension OverviewViewController: UITextFieldDelegate {

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        deselectSelectedCell()
        return false
    }

}