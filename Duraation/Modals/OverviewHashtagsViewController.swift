//
//  OverviewHashtagsViewController.swift
//  Duraation
//
//  Created by Emad A. on 9/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class OverviewHashtagsViewController: UIViewController {

    private let tableViewCellId = "HashtagCell"

    // MARK: - Public Properties

    @IBOutlet weak var navView: ModalNavigationView?
    @IBOutlet weak var tableView: UITableView?

    weak var overview: Overview? {
        didSet {
            selectedHashtags = overview!.hashtags
        }
    }

    // MARK: - Private Properties

    private var sections = [String]()
    private var hashtags = [String: [String]]()

    private var selectedHashtags = [String]()

    // MARK: Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // All hashtags sorted alphabetically
        let allHashtags = DataCenter.defaultCenter().getHashtags()
        let digits = NSCharacterSet.letterCharacterSet()
        // Go through all haghtags ...
        for hashtag in allHashtags {
            // ... and get the first character as section.
            var section = String(hashtag[hashtag.startIndex])

            // If the section is not letter put it in specific group.
            let unicode = section.unicodeScalars
            if digits.longCharacterIsMember(unicode[unicode.startIndex].value) == false {
                section = ":\\"
            }

            // If the section has not already captured ...
            if contains(sections, section) == false {
                // ... create a section for that
                sections.append(section)
                hashtags[section] = [String]()
            }
            // Add the hashtag into related section
            hashtags[section]!.append(hashtag)
        }

        // Setting navigation view
        navView?.doneButtonHidden = false
        navView?.titleLabel.text = "Hashtags"
        navView?.doneButton.addTarget(
            self,
            action: Selector("backToRootViewController"),
            forControlEvents: .TouchUpInside)

        // Setting table view
        tableView?.tableFooterView = UIView()
        tableView?.layoutMargins = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)

        if allHashtags.count == 0 {
            if let tableView = tableView {
                let emptyListLabel = EmptyTableViewLabel(frame: CGRect(
                    origin: CGPoint(x: CGRectGetMinX(tableView.bounds), y: CGRectGetMinY(tableView.bounds)),
                    size: CGSize(width: CGRectGetWidth(tableView.bounds), height: 140)))
                emptyListLabel.text = "No hashtag found"
                tableView.addSubview(emptyListLabel)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Private Methods

    func backToRootViewController() {
        overview?.hashtags = selectedHashtags
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
}

extension OverviewHashtagsViewController: UITableViewDataSource {

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hashtags[sections[section]]!.count
    }

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if overview == nil {
            return
        }

        // Set options selected if hashtag is on the array
        let hashtag = hashtags[sections[indexPath.section]]![indexPath.row]
        (cell as! EntryFormSelectionalCell).optionSelected = contains(selectedHashtags, hashtag)
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(tableViewCellId) as? EntryFormSelectionalCell
        if cell == nil {
            cell = EntryFormSelectionalCell(style: .Default, reuseIdentifier: tableViewCellId)
            cell!.textLabel?.font = ApplicationFontStyle.MainTitle.toFont()
            cell!.textLabel?.textColor = UIColor.mineShaftColor()
        }

        cell!.textLabel?.text = "#" + hashtags[sections[indexPath.section]]![indexPath.row]

        return cell!
    }

}

extension OverviewHashtagsViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 26
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 54
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 1))
        view.backgroundColor = UIColor.blackSqueeze()

        // To cover the previous cell bloody separator
        let hairLineHeight: CGFloat = 1 / UIScreen.mainScreen().scale
        let antiseparator = UIView(frame: CGRect(
            origin: CGPoint(x: CGRectGetMinX(view.bounds), y: CGRectGetMinY(view.bounds) - hairLineHeight),
            size: CGSize(width: CGRectGetWidth(view.bounds), height: hairLineHeight)))
        antiseparator.backgroundColor = view.backgroundColor
        antiseparator.autoresizingMask = .FlexibleWidth
        view.addSubview(antiseparator)

        // The header title label
        let label = UILabel(frame: CGRectInset(view.bounds, tableView.layoutMargins.left, 0))
        label.text = String(sections[section]).uppercaseString
        label.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        label.font = ApplicationFontStyle.TeenyTiny.toFont()
        label.textColor = UIColor.aluminumColor()
        view.addSubview(label)

        return view
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if overview == nil {
            return
        }

        let cell = tableView.cellForRowAtIndexPath(indexPath) as! EntryFormSelectionalCell
        let hashtag = hashtags[sections[indexPath.section]]![indexPath.row]

        // Add the selected hashtag in the array
        if contains(selectedHashtags, hashtag) == false {
            selectedHashtags.append(hashtag)
        }
        // Remove the selected hashtag from the array
        else if let index = find(selectedHashtags, hashtag) {
            selectedHashtags.removeAtIndex(index)
        }
    }

}