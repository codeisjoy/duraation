//
//  DataCenter.swift
//  Duraation
//
//  Created by Emad A. on 14/04/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import SQLite

// MARK: - Table Models
// MARK: -

private typealias XRefs = (left: Expression<Int64>, right: Expression<Int64>)

private class TableModel {

    // MARK: Properties

    private(set) var table: Query

    // MARK: - Methods

    init(db: Database) {
        table = db[""]
    }

}

private class ActivitiesTableModel: TableModel {

    // MARK: - Columns

    let uid   = Expression<Int64>("uid")
    let start = Expression<NSDate>("start")
    let end   = Expression<NSDate>("end")
    let note  = Expression<String>("note")

    // MARK: - Methods

    override init(db: Database) {
        super.init(db: db)

        table = db["activities"]
        db.create(table: table, ifNotExists: true) { t in
            t.column(uid, primaryKey: .Autoincrement)
            t.column(start)
            t.column(end)
            t.column(note)
        }
    }
    
}

private class HashtagsTableModel: TableModel {

    // MARK: - Columns

    let uid  = Expression<Int64>("uid")
    let text = Expression<String>("text")

    // MARK: - Methods

    override init(db: Database) {
        super.init(db: db)

        table = db["hashtags"]
        db.create(table: table, ifNotExists: true) { t in
            t.column(uid, primaryKey: .Autoincrement)
            t.column(text, unique: true, collate: .Nocase)
        }
    }

}

private class AcTagsTableModel: TableModel {

    // MARK: - Columns

    let uid      = Expression<Int64>("uid")
    let activity = Expression<Int64?>("activity_uid")
    let hashtag  = Expression<Int64?>("hashtag_uid")

    // MARK: - Methods

    init(db: Database, xref: XRefs) {
        super.init(db: db)

        table = db["activities_hashtags"]
        db.create(table: table, ifNotExists: true) { t in
            t.column(uid, primaryKey: .Autoincrement)
            t.column(activity)
            t.column(hashtag)

            t.foreignKey(activity, references: xref.left, update: .NoAction, delete: .Cascade)
            t.foreignKey(hashtag, references: xref.right, update: .NoAction, delete: .Cascade)
        }
    }

}

private class OverviewsTableModel: TableModel {

    // MARK: - Columns

    let uid             = Expression<Int64>("uid")
    let title           = Expression<String>("title")
    let dateRangeType   = Expression<Int64>("date_range_type")
    let hashtags        = Expression<NSArray>("hashtags")

    // MARK: - Methods

    override init(db: Database) {
        super.init(db: db)

        table = db["overviews"]
        db.create(table: table, ifNotExists: true) { t in
            t.column(uid, primaryKey: .Autoincrement)
            t.column(title)
            t.column(dateRangeType)
            t.column(hashtags)
        }
    }

}

private class OverviewCustomDateRangeTableModel: TableModel {

    // MARK: - Columns

    let uid      = Expression<Int64>("uid")
    let from     = Expression<NSDate>("from")
    let to       = Expression<NSDate>("to")
    let overview = Expression<Int64>("overview_uid")

    // MARK: - Methods

    init(db: Database, overviewRef: Expression<Int64>) {
        super.init(db: db)

        table = db["overview_custom_date_range"]
        db.create(table: table, ifNotExists: true) { t in
            t.column(uid, primaryKey: .Autoincrement)
            t.column(from)
            t.column(to)
            t.column(overview)

            t.foreignKey(overview, references: overviewRef, update: .NoAction, delete: .Cascade)
        }
    }
}

// MARK: - DataCenter
// MARK: -

class DataCenter {

    class func defaultCenter() -> DataCenter {
        struct Static {
            static let instance: DataCenter = DataCenter()
        }
        return Static.instance
    }

    // MARK: - Tables

    private let activities: ActivitiesTableModel
    private let hashtags: HashtagsTableModel
    private let actags: AcTagsTableModel
    private let overviews: OverviewsTableModel
    private let overviewsCustomDateRange: OverviewCustomDateRangeTableModel

    // MARK: - Private Properties

    // The queue all transaction will be run in
    private let queue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

    // An instance of SQLite db
    private let db: Database = {
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as! String
        let db = Database("\(path)/db.sqlite3")
        db.foreignKeys = true
        #if DEBUG
            println(db)
        #endif
        return db
        }()

    // MARK: -

    init() {
        activities = ActivitiesTableModel(db: self.db)
        hashtags = HashtagsTableModel(db: self.db)

        let actagsXRef = (left: activities.table[activities.uid], right: hashtags.table[hashtags.uid])
        actags = AcTagsTableModel(db: db, xref: actagsXRef)

        overviews = OverviewsTableModel(db: db)
        overviewsCustomDateRange = OverviewCustomDateRangeTableModel(db: db, overviewRef: overviews.table[overviews.uid])
    }

    // Mark: - Public Methods

    func performBlock(block: () -> Void) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.25 * Double(NSEC_PER_SEC))), queue, block)
    }

    func getActivities(forDate date: NSDate) -> [Activity] {
        var result = [Activity]()

        // Make sure given date starts from midnight so remove time units.
        let units: NSCalendarUnit = .CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay
        let components = NSCalendar.currentCalendar().components(units, fromDate: date)

        if let date = NSCalendar.currentCalendar().dateFromComponents(components) {
            // Start from given midnight to tomorrow midnight
            let fromMidnight: NSDate = date
            let toMidnight: NSDate = fromMidnight.dateByAddingTimeInterval(24 * 60 * 60)
            // Filter activities which their start or end dates are in the period
            let condition =
                (activities.start >= fromMidnight && activities.start < toMidnight) ||
                (activities.end   >= fromMidnight && activities.end   < toMidnight) ||
                (activities.start <= fromMidnight && activities.end   > toMidnight)
            // Fetch proper rows
            let rows = activities.table.filter(condition).order(activities.start.desc, activities.end.desc, activities.uid.desc)
            // Convert fetched rows into appropriate format
            for row in rows {
                let model = Activity()
                model.uid = row.get(activities.uid)
                model.start = row.get(activities.start)
                model.end = row.get(activities.end)
                model.note = row.get(activities.note)
                result.append(model)
            }
        }

        return result
    }

    func getOverviews() -> [Overview] {
        var result = [Overview]()

        // Left outer join gives all wanted rows
        // even if there is no item in custom date range.
        let rows = overviews.table.join(
            .LeftOuter,
            overviewsCustomDateRange.table,
            on: overviewsCustomDateRange.overview == overviews.table[overviews.uid])
            .order(overviews.uid.desc)

        // Convert fetched rows into appropriate format
        for row in rows {
            let model = Overview()
            model.uid = row.get(overviews.table.namespace(overviews.uid))
            model.title = row.get(overviews.table.namespace(overviews.title))
            model.dateRangeType = OverviewDateRange(rawValue: row.get(overviews.table.namespace(overviews.dateRangeType))) ?? .None
            model.hashtags = row.get(overviews.table.namespace(overviews.hashtags)) as? Array<String> ?? Array<String>()
            if model.dateRangeType == OverviewDateRange.Custom {
                model.customDateRange = (
                    from: row.get(overviewsCustomDateRange.table.namespace(overviewsCustomDateRange.from)),
                    to: row.get(overviewsCustomDateRange.table.namespace(overviewsCustomDateRange.to))
                )
            }
            
            result.append(model)
        }

        return result
    }

    func getHashtags() -> [String] {
        var result = [String]()
        let rows = hashtags.table.order(hashtags.text.asc, hashtags.uid.asc)
        for row in rows {
            result.append(row.get(hashtags.text))
        }

        return result
    }

    func save(model: DBModelObject, completion: (Bool -> Void)?) {
        var result = false
        performBlock { [unowned self] in
            // Save or update the given activity object.
            if model is Activity {
                result = self.saveActivity(model as! Activity)
            }
            // Save the given overview object.
            else if model is Overview {
                result = self.saveOverview(model as! Overview)
            }
            else {
                return
            }

            dispatch_async(dispatch_get_main_queue()) {
                completion?(result)
            }
        }
    }

    func delete(model: DBModelObject, completion: (Bool -> Void)?) {
        var result = false
        performBlock {
            // Remove the given activity object.
            if model is Activity {
                result = self.deleteActivity(model as! Activity)
            }
            else if model is Overview {
                result = self.deleteOverview(model as! Overview)
            }
            else {
                return
            }

            dispatch_async(dispatch_get_main_queue()) {
                completion?(result)
            }
        }
    }

    func getActivitiesContain(tags: [String], dateRange: (from: NSDate, to: NSDate)?, completion: [Activity] -> Void) {
        performBlock { [weak self] in
            if let sself = self {
                var result = [Activity]()

                // Defining conditions
                let tagsCondition = contains(tags, sself.hashtags.table[sself.hashtags.text])
                var timeCondition = Expression<Bool>(value: true)
                if let range = dateRange {
                    timeCondition =
                        (sself.activities.start >= range.from && sself.activities.start < range.to) ||
                        (sself.activities.end   >= range.from && sself.activities.end   < range.to) ||
                        (sself.activities.start <= range.from && sself.activities.end   > range.to)
                }

                // Run the query
                let rows = sself.activities.table.select(distinct: sself.activities.table[*])
                    .join(sself.actags.table, on: sself.actags.activity == sself.activities.table[sself.activities.uid])
                    .join(sself.hashtags.table, on: sself.hashtags.table[sself.hashtags.uid] == sself.actags.table[sself.actags.hashtag])
                    .filter(tagsCondition && timeCondition)
                    .order(sself.activities.start.desc)

                // Convert the selected rows to proper model
                for row in rows {
                    let model   = Activity()
                    model.uid   = row.get(sself.activities.uid)
                    model.start = row.get(sself.activities.start)
                    model.end   = row.get(sself.activities.end)
                    model.note  = row.get(sself.activities.note)

                    result.append(model)
                }

                completion(result)
            }
        }
    }

    // MARK: - Private Activity Related Methods

    private func saveActivity(activity: Activity) -> Bool {
        var result = false;
        // Prepare the values of given activity to be inserted to db.
        var aValues = Array<Setter>()
        if let uid = activity.uid where uid > 0 {
            aValues.append(activities.uid <- uid)
        }
        aValues.append(activities.start <- activity.start)
        aValues.append(activities.end   <- activity.end)
        aValues.append(activities.note  <- activity.note)

        // Begin db transaction.
        db.transaction { tnx in
            // Insert the activity values or update - if the uid is given - and get the row id.
            if let aRowId = activities.table.insert(or: .Replace, aValues).rowid {
                // In case it is an update ...
                if aRowId == activity.uid {
                    // ... remove all current activity-hashtags references, to insert new ones.
                    let delete = actags.table.filter(actags.activity == aRowId).delete()
                    if delete.changes == nil {
                        return .Rollback
                    }
                }
                if let allHashtags = activity.hashtags {
                    // For all hashtags in the activity note ...
                    for hashtag in allHashtags {
                        // ... get the row id of ...
                        var hRowId: Int64? =
                            // ... existing hashtag, or ...
                            hashtags.table.select(hashtags.uid).filter(hashtags.text == hashtag).first?.get(hashtags.uid) ??
                            // ... inserted new hashtag
                            hashtags.table.insert(hashtags.text <- hashtag).rowid
                        // Skip and go for the next, if there was an error.
                        if hRowId == nil {
                            continue
                        }
                        // Insert an activity-hashtag reference.
                        let insert = actags.table.insert(actags.activity <- aRowId, actags.hashtag <- hRowId!)
                        if insert.rowid == nil {
                            return .Rollback
                        }
                    }
                }
                // Remove all hashtags which are not being used for any activity.
                cleanupUnusedHashtags()
                // All good. Let's commit the transaction.
                result = true
                return .Commit
            }
            // Something went wrong.
            return .Rollback
        }
        
        return result
    }

    private func deleteActivity(activity: Activity) -> Bool {
        // Select the activity with given uid.
        let object = activities.table.filter(activities.uid == activity.uid!)
        // If there was any, delete it.
        if object.count > 0 && object.delete().changes != nil {
            // Remove all hashtags which are not being used for any activity.
            cleanupUnusedHashtags()
            return true
        }
        return false
    }

    private func cleanupUnusedHashtags() -> Change {
        // Create a list of all hashtags uid in activity-hashtag reference table.
        let allXRefHashtags = actags.table.select(distinct: actags.hashtag)
        // Remove all hashtags are in hashtags table but not in activity-hashtag reference table.
        return hashtags.table.filter(!contains(allXRefHashtags, hashtags.uid)).delete()
    }

    // MARK: - Private Overview Related Methods

    private func saveOverview(overview: Overview) -> Bool {
        var result = false;
        // Prepare the values of overview occording to given object
        var oValues = Array<Setter>()
        oValues.append(overviews.title <- overview.title)
        oValues.append(overviews.dateRangeType <- overview.dateRangeType.rawValue)
        oValues.append(overviews.hashtags <- NSArray(array: overview.hashtags, copyItems: true))

        var dValue: Array<Setter>?
        // If it is needed to set a custom date range prepare the values.
        if overview.dateRangeType == OverviewDateRange.Custom {
            dValue = Array<Setter>()
            dValue!.append(overviewsCustomDateRange.from <- overview.customDateRange!.from!)
            dValue!.append(overviewsCustomDateRange.to <- overview.customDateRange!.to!)
        }

        // Begin db transaction.
        db.transaction { tnx in
            // Insert the overview row and get the row id.
            if let rowId = overviews.table.insert(oValues).rowid {
                // If custom dare range was needed to be inserted ...
                if dValue != nil {
                    // ... get a reference of inserted row ...
                    dValue!.append(overviewsCustomDateRange.overview <- rowId)
                    // ... and try to save it or ignor all changes.
                    if overviewsCustomDateRange.table.insert(dValue!).rowid == nil {
                        return .Rollback
                    }
                }
                // All good. Let's commit the transaction.
                result = true
                return .Commit
            }
            // Something went wrong.
            return .Rollback
        }

        return result
    }

    private func deleteOverview(overview: Overview) -> Bool {
        // Select the overview with given uid.
        let object = overviews.table.filter(overviews.uid == overview.uid!)
        // If there was any, delete it.
        return object.count > 0 && object.delete().changes != nil
    }

}
