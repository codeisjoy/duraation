//
//  Activity.swift
//  Duraation
//
//  Created by Emad A. on 6/04/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import SQLite

class DBModelObject {
    var uid: Int64?
}

class Activity: DBModelObject {

    var start: NSDate = NSDate(timeIntervalSinceNow: 60 * 60 * -1)
    var end: NSDate = NSDate(timeIntervalSinceNow: 0)
    var note: String = ""
    var hashtags: [String]?

    var duration: NSTimeInterval {
        return end.timeIntervalSince1970 - start.timeIntervalSince1970
    }

}

class Overview: DBModelObject {
    
    var title: String = "Untitled Overview"
    var dateRangeType: OverviewDateRange = .None
    var customDateRange: (from: NSDate?, to: NSDate?)?
    var hashtags = [String]()

}

enum OverviewDateRange: Int64 {

    case
    All        = 1,
    Last60Days = 2,
    Last28Days = 3,
    Last7Days  = 4,
    Custom     = 5,
    None       = 0

    func toString() -> String {
        switch self {
        case .All:        return "All time"
        case .Last60Days: return "Last 60 days"
        case .Last28Days: return "Last 28 days"
        case .Last7Days:  return "Last 7 days"
        case .Custom:     return "Custom"
        default:          return ""
        }
    }

    func toDateRageFromNow() -> (from: NSDate, to: NSDate)? {
        if (self == .All || self == .None || self == .Custom) {
            return nil
        }

        let calendar = NSCalendar.currentCalendar()
        calendar.locale = NSLocale(localeIdentifier: "en_AU")

        let today = calendar.components(
            .CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay |
                .CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitSecond,
            fromDate: NSDate())

        let from = calendar.components(.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay, fromDate: NSDate())
        let to   = calendar.components(.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay, fromDate: NSDate())
        to.second = -1
        to.day += 1

        var dateRange: (from: NSDate, to: NSDate)
        if self == .Last60Days {
            from.day -= 60
        }
        else if self == .Last28Days {
            from.day -= 28
        }
        else if self == .Last7Days {
            from.day -= 7
        }

        dateRange = (
            from: calendar.dateFromComponents(from)!,
            to: calendar.dateFromComponents(to)!)
        
        return dateRange
    }
}

/*
class Activity: RLMObject {

    dynamic var uid: String = NSUUID().UUIDString
    dynamic var start: NSTimeInterval = 0
    dynamic var end: NSTimeInterval = 0
    dynamic var note: String = ""
    dynamic var hashtags: RLMArray = RLMArray(objectClassName: Hashtag.className())

    var duration: UInt {
        return UInt(round(end - start) / 60)
    }

    override class func primaryKey() -> String {
        return "uid"
    }

}

class Overview: RLMObject {

    dynamic var uid: String = NSUUID().UUIDString
    dynamic var title: String = "Untitled Overview"
    dynamic var dateRangeType: Int = OverviewDateRange.None.rawValue
    dynamic var customDateRangeFrom: NSTimeInterval = 0
    dynamic var customDateRangeTo: NSTimeInterval = 0
    dynamic var hashtags: RLMArray = RLMArray(objectClassName: Hashtag.className())
    
    override class func primaryKey() -> String {
        return "uid"
    }

}

class Hashtag: RLMObject {

    dynamic var uid: String = NSUUID().UUIDString
    dynamic var text: String = ""
    dynamic var activities: [Activity] {
        return linkingObjectsOfClass("Activity", forProperty: "hashtags") as! [Activity]
    }

    override class func primaryKey() -> String {
        return "uid"
    }
    
}
*/