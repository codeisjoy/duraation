//
//  SQLiteCustomTypes.swift
//  Duraation
//
//  Created by Emad A. on 13/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import SQLite
import UIKit

let SQLDateFormatter: NSDateFormatter = {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
    formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
    return formatter
    }()

extension NSDate: Value {

    public class var declaredDatatype: String {
        return String.declaredDatatype
    }

    public class func fromDatatypeValue(stringValue: String) -> NSDate {
        return SQLDateFormatter.dateFromString(stringValue)!
    }

    public var datatypeValue: String {
        return SQLDateFormatter.stringFromDate(self)
    }

}

extension NSData: Value {

    public class var declaredDatatype: String {
        return Blob.declaredDatatype
    }

    public class func fromDatatypeValue(blobValue: Blob) -> Self {
        return self(bytes: blobValue.bytes, length: blobValue.length)
    }

    public var datatypeValue: Blob {
        return Blob(bytes: bytes, length: length)
    }
}

extension NSArray: Value {

    private static let separator: String = "\n"

    public class var declaredDatatype: String {
        return String.declaredDatatype
    }

    public class func fromDatatypeValue(stringValue: String) -> NSArray {
        return stringValue.componentsSeparatedByString(separator)
    }

    public var datatypeValue: String {
        return self.componentsJoinedByString(NSArray.separator)
    }
}