//
//  Utils.swift
//  Duraation
//
//  Created by Emad A. on 30/12/2014.
//  Copyright (c) 2014 Emad A. All rights reserved.
//

import UIKit

// MARK: - Application Colours
// MARK:

extension UIColor {

    class func whiteSmokeColor() -> UIColor {
        return UIColor(red:0.96, green:0.96, blue:0.96, alpha:1)
    }

    class func concreteColor() -> UIColor {
        return UIColor(red:0.76, green:0.76, blue:0.76, alpha:1)
    }

    class func aluminumColor() -> UIColor {
        return UIColor(red:0.6, green:0.6, blue:0.6, alpha:1)
    }

    class func mineShaftColor() -> UIColor {
        return UIColor(red:0.2, green:0.2, blue:0.2, alpha:1)
    }
    
    class func appleGreenColor() -> UIColor {
        return UIColor(red:0.46, green:0.71, blue:0.29, alpha:1)
    }

    class func summerSkyColor() -> UIColor {
        return UIColor(red:0.26, green:0.66, blue:0.88, alpha:1)
    }

    class func blackSqueeze() -> UIColor {
        return UIColor(red:0.96, green:0.98, blue:0.98, alpha:1)
    }

    class func jasperColor() -> UIColor {
        return UIColor(red:0.84, green:0.26, blue:0.26, alpha:1)
    }
}

// MARK: - Application Fonts
// Mark:

enum ApplicationFontStyle: Int {
    case NavTitle, LightFifteen, MainTitle, TeenyTiny, Body, HugeLight, SmallHeavy
    func toFont() -> UIFont {
        switch self {
        case .NavTitle:
            return UIFont(name: "Avenir-Black", size: 17)!
        case .LightFifteen:
            return UIFont(name: "Avenir-Light", size: 15)!
        case .MainTitle:
            return UIFont(name: "Avenir-Medium", size: 17)!
        case .TeenyTiny:
            return UIFont(name: "Avenir-Book", size: 11)!
        case .Body:
            return UIFont(name: "Avenir-Book", size: 15)!
        case .HugeLight:
            return UIFont(name: "Avenir-Light", size: 55)!
        case .SmallHeavy:
            return UIFont(name: "Avenir-Heavy", size: 14)!
        default:
            return UIFont.systemFontOfSize(16)
        }
    }
}

// MARK: - Array Extension
// MARK:

extension Array {
    func sample() -> T {
        let index = arc4random_uniform(UInt32(count))
        return self[Int(index)]
    }
}