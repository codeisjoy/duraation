//
//  DurationViewController.swift
//  Duraation
//
//  Created by Emad A. on 22/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class DurationViewController: UIViewController {

    private let tableViewCellId = "ActivityCell"

    // MARK: - Properties

    // Keeps an instance of overview has been passed to.
    var overview: Overview = Overview() {
        didSet { overviewDidSet() }
    }

    @IBOutlet weak var navigationBar: UINavigationBar?
    @IBOutlet weak var navigationBarItem: UINavigationItem?

    @IBOutlet weak var tableView: UITableView?

    // MARK: - Private Properties

    private var activities: [Activity]?

    // Custom navigation bar title view
    private let titleView = DurationNavigationItemTitleView()

    // The view above the table view shows the overview info and total duration amount
    private let summaryView = DurationSummaryView()

    // Just a container for summary view
    private let summaryViewContainer = UIView()

    private let emptyListLabel = EmptyTableViewLabel(frame: CGRectZero)

    // MARK: - Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // A view as status bar background
        let statusBarView = UIView(frame: UIApplication.sharedApplication().statusBarFrame)
        statusBarView.backgroundColor = UIColor.appleGreenColor()
        view.insertSubview(statusBarView, atIndex: 0)

        // Config navigation bar
        navigationBar?.shadowImage = UIImage()
        navigationBar?.barTintColor = UIColor.appleGreenColor()
        navigationBar?.setBackgroundImage(UIImage(), forBarMetrics: .Default)

        // Config navigation item
        navigationBarItem?.titleView = titleView
        navigationBarItem?.leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "icon-back"),
            style: UIBarButtonItemStyle.Plain,
            target: self,
            action: Selector("popViewController"))

        // Config table view
        tableView?.estimatedRowHeight = 100
        tableView?.tableFooterView = UIView()
        tableView?.layoutMargins = UIEdgeInsetsZero
        tableView?.separatorInset = UIEdgeInsetsZero
        tableView?.separatorColor = UIColor.concreteColor()
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.registerNib(
            UINib(nibName: "ActivityTableViewCell", bundle: NSBundle.mainBundle()),
            forCellReuseIdentifier: tableViewCellId)

        // The table header is a long view above the table,
        // with the same colour background as the summary view
        // to make a seamless summary view.
        if let tableView = tableView {
            let tableHeader = UIView()
            tableHeader.frame = CGRect(
                origin: CGPoint(x: CGRectGetMinX(tableView.bounds), y: CGRectGetMaxY(tableView.bounds) * -1),
                size: tableView.bounds.size)
            tableHeader.backgroundColor = UIColor.appleGreenColor()
            tableHeader.autoresizingMask = .FlexibleWidth
            tableView.addSubview(tableHeader)
        }
        // This is important that content inset apply after adding table header.
        tableView?.contentInset = UIEdgeInsets(top: 210, left: 0, bottom: 0, right: 0)

        if let tableView = tableView {
            emptyListLabel.text = "No activity found"
            emptyListLabel.frame = CGRect(
                origin: CGPoint(x: CGRectGetMinX(tableView.bounds), y: CGRectGetMinY(tableView.bounds) + tableView.contentInset.top),
                size: CGSize(width: CGRectGetWidth(tableView.bounds), height: 140))
            tableView.addSubview(emptyListLabel)
        }

        // If summary view is added above the table view then it covers the scroll indicator.
        // So, there is no option but having a edge inset to make sure nothing cover the indicator.
        summaryView.frame = UIEdgeInsetsInsetRect(summaryViewContainer.bounds, UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10))
        summaryViewContainer.addSubview(summaryView)
        summaryViewContainer.clipsToBounds = true
        view.addSubview(summaryViewContainer)

        // Tracking the view controller
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Duration")

        let builder  = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject: AnyObject])
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        titleView.setTitle(overview.title, prompt: "0h 00m")

        // Set the frame of title view.
        var frame = navigationBarItem?.titleView?.frame
        frame?.size = CGSize(width: view.bounds.width * 0.7, height: 44)
        titleView.frame = frame ?? CGRectZero
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    // MARK: - Private Methods

    @objc private func popViewController() {
        navigationController?.popViewControllerAnimated(true)
    }

    private func overviewDidSet() {
        var dateRange: (from: NSDate, to: NSDate)?
        if overview.dateRangeType == .Custom {
            if let
                to = overview.customDateRange?.to,
                from = overview.customDateRange?.from
            {
                dateRange = (from: from, to: to)
            }
        }
        else {
            dateRange = overview.dateRangeType.toDateRageFromNow()
        }

        summaryView.updateDateRangeLabel(string: overview.dateRangeType.toString(), range: dateRange)
        summaryView.updateHashtagsLabel(hashtags: overview.hashtags, showAll: {
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AllHashtags") as? OverviewAllHashtagsViewController {
                vc.allHashtags = self.overview.hashtags
                let modal = EMPartialModalViewController(rootViewController: vc, contentHeight: self.view.bounds.width)
                self.presentViewController(modal, animated: true, completion: nil)
            }
        })

        let dataCenter = DataCenter.defaultCenter()
        dataCenter.getActivitiesContain(overview.hashtags, dateRange: dateRange) { [weak self] (result: [Activity]) in
            if let sself = self {
                var duration: Double = 0
                for activity in result {
                    var period = (a: activity.start.timeIntervalSince1970, b: activity.end.timeIntervalSince1970)
                    if let range = dateRange {
                        period.a = max(period.a, range.from.timeIntervalSince1970)
                        period.b = min(period.b, range.to.timeIntervalSince1970)
                    }
                    duration += max(period.b - period.a, 0)
                }
                // Convert duration seconds to minutes
                duration /= 60

                let h = UInt(floor(duration / 60))
                let m = UInt(ceil(duration % 60))

                sself.activities = result
                dispatch_async(dispatch_get_main_queue()) {
                    var prompt = "\(h)h "
                    if m < 10 { prompt += "0" }
                    prompt += "\(m)m"
                    sself.titleView.setTitle(sself.overview.title, prompt: prompt)

                    sself.summaryView.updateDurationLabel(hours: h, minutes: m)
                    sself.tableView?.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)

                    if result.count > 0 {
                        sself.emptyListLabel.removeFromSuperview()
                    }
                }
            }
        }
    }

}

extension DurationViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activities?.count ?? 0
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: ActivityTableViewCell = tableView.dequeueReusableCellWithIdentifier(tableViewCellId) as! ActivityTableViewCell
        cell.layoutMargins = tableView.layoutMargins

        cell.updateViewWithActivity(activities![indexPath.row])

        return cell
    }

}

// MARK: - UIScrollViewDelegate
// MARK: -

extension DurationViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(scrollView: UIScrollView) {
        let topInset = scrollView.contentInset.top
        let topOffset = abs(min(scrollView.contentOffset.y, 0))

        // Setting table header position ...
        
        // Summary view container should be as long as scroll view vertical offset
        // to cover the scroll view top content inset
        summaryViewContainer.frame = CGRect(
            origin: scrollView.frame.origin,
            size: CGSize(width: view.bounds.width, height: topOffset))

        // Summary view position will change dramatically according to scroll view vertical offset
        summaryView.center = CGPoint(
            x: CGRectGetMidX(view.bounds),
            y: topInset / 2 - (topInset - topOffset) * 0.14)

        // Setting custom title view labels position ...

        // The value of scroll view vertica offset in which title view labels should start moving
        let hotPointY = titleView.bounds.height * 1.5
        // The portion of scroll view offset changes
        let ratio = topOffset / hotPointY
        // The title label definitions
        var titleLabelFrame = titleView.titleLabel.frame
        let titleLabelMinY = CGRectGetMinY(titleView.bounds)
        let titleLabelMaxY = (titleView.bounds.height - titleView.titleLabel.bounds.height) / 2
        // The prompt label definitions
        var promptLabelFrame = titleView.promptLabel.frame
        let promptLabelMinY = CGRectGetMaxY(titleView.bounds) - promptLabelFrame.height - 2
        let promptLabelMaxY = CGRectGetMaxY(titleView.bounds)
        // If scroll view vertical offset is in hot area ...
        if topOffset < hotPointY {
            // ... set the title and prompt label position ...
            titleLabelFrame.origin.y = (titleLabelMaxY - titleLabelMinY) * ratio + titleLabelMinY
            promptLabelFrame.origin.y = (promptLabelMaxY - promptLabelMinY) * ratio + promptLabelMinY
            // ... and change the prompt label alpha accordingly.
            titleView.promptLabel.alpha = 1 - ratio * 2.8
        }
        // If the scroll view vertical offset is not in hot area ...
        else {
            // ... set the default position and alpha for title and prompt label.
            titleLabelFrame.origin.y = titleLabelMaxY
            promptLabelFrame.origin.y = promptLabelMaxY
            titleView.promptLabel.alpha = 0
        }
        
        titleView.titleLabel.frame = titleLabelFrame
        titleView.promptLabel.frame = promptLabelFrame
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        var contentOffset = scrollView.contentOffset
        if contentOffset.y > (titleView.bounds.height * -1.5) && contentOffset.y < 0 {
            contentOffset.y = 0
            scrollView.setContentOffset(contentOffset, animated: true)
        }
    }

    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        var contentOffset = targetContentOffset.memory
        if contentOffset.y > (titleView.bounds.height * -1.5) && contentOffset.y < 0 {
            contentOffset.y = 0
            scrollView.setContentOffset(contentOffset, animated: true)
        }
    }

}

// MARK: -
// MARK: -

class OverviewAllHashtagsViewController: UIViewController {

    private let tableViewCellId = "HashtagCell"

    // MARK: - Public Properties

    var allHashtags = [String]()

    @IBOutlet weak var navView: ModalNavigationView?
    @IBOutlet weak var tableView: UITableView?

    // MARK: -

    // MARK: - Private Properties

    private var sections = [String]()
    private var hashtags = [String: [String]]()

    // MARK: - Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        let digits = NSCharacterSet.letterCharacterSet()
        // Go through all haghtags ...
        for hashtag in allHashtags {
            // ... and get the first character as section.
            var section = String(hashtag[hashtag.startIndex])

            // If the section is not letter put it in specific group.
            let unicode = section.unicodeScalars
            if digits.longCharacterIsMember(unicode[unicode.startIndex].value) == false {
                section = ":\\"
            }

            // If the section has not already captured ...
            if contains(sections, section) == false {
                // ... create a section for that
                sections.append(section)
                hashtags[section] = [String]()
            }
            // Add the hashtag into related section
            hashtags[section]!.append(hashtag)
        }

        // Setting navigation view
        navView?.doneButtonHidden = false
        navView?.titleLabel.text = "Hashtags"
        navView?.doneButton.addTarget(
            self,
            action: Selector("dismiss"),
            forControlEvents: .TouchUpInside)

        // Setting table view
        tableView?.tableFooterView = UIView()
        tableView?.separatorColor = UIColor.concreteColor()
        tableView?.layoutMargins = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)
    }

    @objc private func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

extension OverviewAllHashtagsViewController: UITableViewDataSource {

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hashtags[sections[section]]!.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(tableViewCellId) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: tableViewCellId)
            cell!.textLabel?.font = ApplicationFontStyle.MainTitle.toFont()
            cell!.textLabel?.textColor = UIColor.mineShaftColor()
        }

        cell!.textLabel?.text = "#" + hashtags[sections[indexPath.section]]![indexPath.row]
        
        return cell!
    }

}

extension OverviewAllHashtagsViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 26
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 54
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 1))
        view.backgroundColor = UIColor.blackSqueeze()

        // The header title label
        let label = UILabel(frame: CGRectInset(view.bounds, tableView.layoutMargins.left, 0))
        label.text = String(sections[section]).uppercaseString
        label.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        label.font = ApplicationFontStyle.TeenyTiny.toFont()
        label.textColor = UIColor.aluminumColor()
        view.addSubview(label)
        
        return view
    }

}