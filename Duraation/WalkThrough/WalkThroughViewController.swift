//
//  WalkThroughViewController.swift
//  
//
//  Created by Emad A. on 20/06/2015.
//
//

import UIKit

let kShowWalkThrough = "ShowWalkThrough"

class WalkThroughViewController: UIViewController {

    private let scrollView = UIScrollView()
    private let pageControl = FXPageControl()
    private let dismissButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.whiteColor()

        scrollView.delegate = self
        scrollView.pagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        view.addSubview(scrollView)

        pageControl.dotSize = 4
        pageControl.dotSpacing = 4
        pageControl.opaque = false
        pageControl.dotColor = UIColor(white: 0, alpha: 0.18)
        pageControl.selectedDotColor = UIColor(white: 0, alpha: 0.40)
        view.addSubview(pageControl)

        dismissButton.layer.borderWidth = 1;
        dismissButton.layer.cornerRadius = 3;
        dismissButton.layer.masksToBounds = true
        dismissButton.layer.borderColor = UIColor.appleGreenColor().CGColor
        dismissButton.titleLabel?.font = ApplicationFontStyle.NavTitle.toFont()
        dismissButton.setTitle("Get Started", forState: .Normal)
        dismissButton.setTitleColor(UIColor.appleGreenColor(), forState: .Normal)
        dismissButton.setTitleColor(UIColor.whiteColor(), forState: .Highlighted)
        dismissButton.setBackgroundImage(imageWithSolidColor(UIColor.appleGreenColor()), forState: .Highlighted)
        dismissButton.addTarget(self, action: Selector("getStarted"), forControlEvents: .TouchUpInside)
        view.addSubview(dismissButton)

        addWalkThroughPages()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        scrollView.frame = view.bounds
        scrollView.contentSize = CGSize(width: scrollView.bounds.width * CGFloat(childViewControllers.count), height: scrollView.bounds.height)
        for var index: Int = 0; index < childViewControllers.count; index++ {
            let page = childViewControllers[index] as! UIViewController
            page.view.frame = CGRect(
                origin: CGPoint(x: scrollView.bounds.width * CGFloat(index), y: CGRectGetMinY(scrollView.bounds)),
                size: scrollView.bounds.size)
        }

        pageControl.frame = CGRect(
            origin: CGPoint(x: CGRectGetMinX(view.bounds), y: floor(view.bounds.height / 1.74) - 16),
            size: CGSize(width: view.bounds.width, height: pageControl.dotSize))

        dismissButton.frame = CGRect(
            origin: CGPoint(x: CGRectGetMinX(view.bounds) + 22, y: CGRectGetMaxY(view.bounds) - (48 + 22)),
            size: CGSize(width: CGRectGetWidth(view.bounds) - 22 * 2, height: 48))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Private Methods

    private func addWalkThroughPages() {
        addWalkThroughPage(
            image: UIImage(named: "walkthrough-1"),
            title: "Welcome to Duraation",
            description: "It seems time management is so important for you. So, let’s see how we can help you in this matter.")
        addWalkThroughPage(
            image: UIImage(named: "walkthrough-2"),
            title: "Define activities",
            description: "Describe what you have done in a period of time by your own hashtags.\n\nLong press to edit or delete an item.")
        addWalkThroughPage(
            image: UIImage(named: "walkthrough-3"),
            title: "Define overviews",
            description: "Create an overview based on available tags and a date range you will choose and ...")
        addWalkThroughPage(
            image: UIImage(named: "walkthrough-4"),
            title: "See the result",
            description: "... that is how much time you have spent on activities contain those hashtags in that period of time.")

        pageControl.numberOfPages = childViewControllers.count
    }

    private func addWalkThroughPage(#image: UIImage?, title: String, description: String) {
        let page = WalkThroughPageViewController(image: image, title: title, description: description)

        addChildViewController(page)
        scrollView.addSubview(page.view)
    }

    @objc private func getStarted() {
        dismissViewControllerAnimated(true) {
            NSUserDefaults().setObject(NSNumber(bool: false), forKey: kShowWalkThrough)
            NSUserDefaults().synchronize()
        }
    }

    private func imageWithSolidColor(color: UIColor) -> UIImage {
        let rect = CGRect(origin: CGPointZero, size: CGSize(width: 2, height: 2))

        UIGraphicsBeginImageContext(rect.size)
        let ctx = UIGraphicsGetCurrentContext()

        CGContextSetFillColorWithColor(ctx, color.CGColor);
        CGContextFillRect(ctx, rect)

        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        return image
    }

}

extension WalkThroughViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(scrollView: UIScrollView) {
        let range: CGFloat = scrollView.contentOffset.x / (scrollView.contentSize.width / CGFloat(childViewControllers.count))
        pageControl.currentPage = Int(round(range))
    }

}
