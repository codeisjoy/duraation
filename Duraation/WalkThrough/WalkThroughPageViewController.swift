//
//  WalkThroughPageViewController.swift
//  
//
//  Created by Emad A. on 20/06/2015.
//
//

import UIKit

class WalkThroughPageViewController: UIViewController {

    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()

    init(image: UIImage?, title: String, description: String) {
        super.init(nibName: nil, bundle: nil)

        imageView.image = image
        titleLabel.text = title
        descriptionLabel.text = description
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.textAlignment = .Center
        titleLabel.textColor = UIColor(white: 0.29, alpha: 1)
        titleLabel.font = ApplicationFontStyle.NavTitle.toFont()

        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .Center
        descriptionLabel.textColor = UIColor(white: 0.46, alpha: 1)
        descriptionLabel.font = ApplicationFontStyle.Body.toFont()

        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let imageDimention = floor(view.bounds.height / 2.5)
        imageView.frame = CGRect(
            origin: CGPoint(x: floor(view.bounds.width - imageDimention) / 2, y: floor(view.bounds.height / 10)),
            size: CGSize(width: imageDimention, height: imageDimention))

        let labelWidth: CGFloat = 275
        let titleLabelSize = titleLabel.sizeThatFits(CGSize(width: labelWidth, height: CGFloat.max))
        titleLabel.frame = CGRect(
            origin: CGPoint(x: floor((view.bounds.width - labelWidth) / 2), y: floor(view.bounds.height / 1.74)),
            size: CGSize(width: 275, height: titleLabelSize.height))

        let descriptionLabelSize = descriptionLabel.sizeThatFits(CGSize(width: labelWidth, height: CGFloat.max))
        descriptionLabel.frame = CGRect(
            origin: CGPoint(x: floor((view.bounds.width - labelWidth) / 2), y: floor(CGRectGetMaxY(titleLabel.frame)) + 10),
            size: CGSize(width: labelWidth, height: descriptionLabelSize.height))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
