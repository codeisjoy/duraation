//
//  HashtagHelper.swift
//  Duraation
//
//  Created by Emad A. on 8/04/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class HashtagHelper {

    // MARK: - Private Constants

    private static let PunctuationChars = "\\-_!\"#$%&'()*+,./:;<=>?@\\[\\]^`{|}~"
    private static let PunctuationCharsWithoutHyphen = "_!\"#$%&'()*+,./:;<=>?@\\[\\]^`{|}~"
    private static let PunctuationCharsWithoutHyphenAndUnderscore = "!\"#$%&'()*+,./:;<=>?@\\[\\]^`{|}~"
    private static let CtrlChars = "\\x00-\\x1F\\x7F"

    private static let HashtagAlpha = "[\\p{L}\\p{M}]"
    private static let HashtagSpecialChars = "_\\u200c\\u200d\\ua67e\\u05be\\u05f3\\u05f4\\u309b\\u309c\\u30a0\\u30fb\\u3003\\u0f0b\\u0f0c\\u0f0d"
    private static let HashtagAlphanumeric = "[\\p{L}\\p{M}\\p{Nd}\(HashtagSpecialChars)]"
    private static let HashtagBoundaryInvalidChars = "&\\p{L}\\p{M}\\p{Nd}\(HashtagSpecialChars)"
    private static let HashtagBoundary = "^|$|[^\(HashtagBoundaryInvalidChars)]"

    private static let ValidHashtag = "(?:\(HashtagBoundary))([#＃]\(HashtagAlphanumeric)*\(HashtagAlpha)\(HashtagAlphanumeric)*)"
    private static let EndHashTagMatch = "\\A(?:[#＃]|://)"

    // MARK: - Public Methods

    static func hashtagsInText(text: String) -> Array<String> {
        let len = count(text)
        if (len < 1) {
            return [];
        }

        var results = Array<String>()
        var position: Int = 0;

        while (true) {
            // Having the first match in range
            let matchResult: NSTextCheckingResult? = self
                .validHashtagRegexp()
                .firstMatchInString(text, options: .WithoutAnchoringBounds, range: NSMakeRange(position, len - position))

            // If there is no match get out
            if matchResult == nil || matchResult?.numberOfRanges < 2 {
                break;
            }

            let hashtagRange: NSRange = matchResult!.rangeAtIndex(1)
            let afterStart: Int = NSMaxRange(hashtagRange);
            var matchOk: Bool = true

            // If match result is not the end of text
            if afterStart < len {
                // Determining the correct range of hashtag
                let endMatchRange: NSRange = self
                    .endHashtagRegexp()
                    .rangeOfFirstMatchInString(text, options: NSMatchingOptions.allZeros, range: NSMakeRange(afterStart, len - afterStart))

                // That's a shame is found nothing.
                if endMatchRange.location != NSNotFound {
                    matchOk = false
                }
            }

            // If match is ok why don't you record it?
            if matchOk {
                var range = NSMakeRange(hashtagRange.location + 1, hashtagRange.length - 1)
                results.append(NSString(string: text).substringWithRange(range))
            }

            // Determining the position for the next loop
            position = NSMaxRange(matchResult!.range);
        }
        
        return results;
    }

    // MARK: - Private Methods

    private static func validHashtagRegexp() -> NSRegularExpression {
        struct Static {
            static var regexp: NSRegularExpression?
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&Static.onceToken) {
            Static.regexp = NSRegularExpression(pattern: self.ValidHashtag, options: .CaseInsensitive, error: nil)
        }

        return Static.regexp!
    }

    private static func endHashtagRegexp() -> NSRegularExpression {
        struct Static {
            static var regexp: NSRegularExpression?
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&Static.onceToken) {
            Static.regexp = NSRegularExpression(pattern: self.EndHashTagMatch, options: .CaseInsensitive, error: nil)
        }

        return Static.regexp!
    }
}