//
//  DateHelper.swift
//  Duraation
//
//  Created by Emad A. on 18/04/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

struct FormattedDateUnit : RawOptionSetType {
    typealias RawValue = UInt
    private var value: UInt = 0
    init(_ value: UInt) { self.value = value }
    init(rawValue value: UInt) { self.value = value }
    init(nilLiteral: ()) { self.value = 0 }
    static var allZeros: FormattedDateUnit { return self(0) }
    static func fromMask(raw: UInt) -> FormattedDateUnit { return self(raw) }
    var rawValue: UInt { return self.value }

    static var Date: FormattedDateUnit { return FormattedDateUnit(1 << 0) }
    static var Time: FormattedDateUnit { return FormattedDateUnit(1 << 1) }
}

class DateHelper {

    private static let dateFormat: String = "dd MMMM yyyy"
    private static let timeFormat: String = "hh:mm a"

    private static var dateFormatter: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.AMSymbol = "AM"
        formatter.PMSymbol = "PM"
        formatter.locale = NSLocale(localeIdentifier: "en_AU")

        return formatter
        }()

    static func formattedStringFromDate(date: NSDate, unitFlags: FormattedDateUnit, extended: Bool = false) -> String {
        var string: String = ""

        if unitFlags & FormattedDateUnit.Date != nil {
            let calendar: NSCalendar = NSCalendar.currentCalendar()
            let units: NSCalendarUnit = .CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay
            let now: NSDate? = calendar.dateFromComponents(calendar.components(units, fromDate: NSDate()))
            let then: NSDate? = calendar.dateFromComponents(calendar.components(units, fromDate: date))

            if now != nil && then != nil {
                let timeIntervalDiff: NSTimeInterval = then!.timeIntervalSinceDate(now!)
                if timeIntervalDiff == 0 {
                    string = "Today"
                }
                else if timeIntervalDiff == 24 * 60 * 60 {
                    string = "Tomorrow"
                }
                else if timeIntervalDiff == 24 * 60 * 60 * -1 {
                    string = "Yesterday"
                }
            }

            if string.isEmpty == true || extended == true {
                dateFormatter.dateFormat = dateFormat
                if string.isEmpty == false {
                    string += ", "
                }
                string += dateFormatter.stringFromDate(date)
            }
        }

        if unitFlags & FormattedDateUnit.Time != nil {
            dateFormatter.dateFormat = timeFormat
            if string.isEmpty == false {
                string += ", "
            }
            string += dateFormatter.stringFromDate(date)
        }

        return string
    }
}
