//
//  OverviewsViewController.swift
//  Duraation
//
//  Created by Emad A. on 1/01/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class OverviewsViewController: UIViewController {

    private let overviewTitleLengthLimit: Int = 25

    // MARK: - Public Properties

    @IBOutlet var tableView: UITableView?

    // MARK: - Private Properties

    private var overviews = [Overview]() {
        didSet {
            if overviews.count == 0 {
                if let tableView = tableView {
                    emptyListLabel.frame = CGRect(
                        origin: CGPoint(x: CGRectGetMinX(tableView.bounds), y: CGRectGetMinY(tableView.bounds)),
                        size: CGSize(width: CGRectGetWidth(tableView.bounds), height: 260))
                }
                tableView?.addSubview(emptyListLabel)
            }
            else {
                emptyListLabel.removeFromSuperview()
            }
        }
    }

    // The outer border of view appears when parent is scrolling
    private lazy var extendedBorderView: UIView = {
        let view: UIView = UIView();
        view.backgroundColor = UIColor(white: 0.85, alpha: 1)
        return view
        }()

    private lazy var dateFormatter: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
        }()

    // Contains a reference to option view controller and related activity
    private var optionsViewController: OptionsViewController?

    private let emptyListLabel = EmptyTableViewLabel(frame: CGRectZero)

    // MARK: - Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Get current overviews for curent date.
        overviews = DataCenter.defaultCenter().getOverviews()
        // Configuring table view.
        if let tableView = tableView {
            tableView.tableFooterView = UIView()
            tableView.layoutMargins = UIEdgeInsetsZero
            tableView.separatorInset = UIEdgeInsetsZero
            tableView.separatorColor = UIColor.concreteColor()
            let hiddenHeader = TableViewHiddenHeader(frame: CGRect(
                origin: CGPoint(x: CGRectGetMinX(tableView.bounds), y: CGRectGetHeight(tableView.bounds) * -1),
                size: CGSize(width: CGRectGetWidth(tableView.bounds), height: CGRectGetHeight(tableView.bounds))),
                context: TableViewHiddenHeaderContext.Overviews)
            tableView.addSubview(hiddenHeader)
        }

        // Adding extented border view beneath all views
        view.insertSubview(extendedBorderView, atIndex: 0);

        emptyListLabel.text = "No overview created\nHit the plus button to add one"
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        if let selectedIndexPath = tableView?.indexPathForSelectedRow() {
            tableView?.deselectRowAtIndexPath(selectedIndexPath, animated: true)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        extendedBorderView.frame = CGRectInset(view.bounds, -1 / UIScreen.mainScreen().scale, 0);
        tableView?.setNeedsLayout()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)

        if segue.identifier == "duration" {
            let destination = segue.destinationViewController as! DurationViewController
            if sender is UITableViewCell {
                if let indexPath = tableView?.indexPathForSelectedRow() {
                    destination.overview = overviews[indexPath.row]
                }
            }
            else if sender is Overview {
                destination.overview = sender as! Overview
            }
        }
    }

    // MARK: - Private Methods

    private func reloadTableView() {
        overviews = DataCenter.defaultCenter().getOverviews()
        tableView?.reloadData()
    }

    private func presentOverviewViewController(forOverview overview: Overview?) {
        let content = storyboard?.instantiateViewControllerWithIdentifier("Overview") as? UINavigationController
        if content == nil { return }

        if let overviewViewController = content!.topViewController as? OverviewViewController {
            // When overview view controller "DONE" button selected ...
            overviewViewController.setDoneAction { [weak self] vc, overview, persistent in
                if let sself = self {
                    let ncenter = NotificationCenter.defaultCenter()
                    // ... validate title ...
                    if overview.title.isEmpty {
                        ncenter.fireNotice(Notice(message: .ErrorOverviewTitleEmpty))
                        return
                    }
                    else if count(overview.title) > sself.overviewTitleLengthLimit {
                        ncenter.fireNotice(Notice(message: .ErrorOverviewTitleTooLong))
                        return
                    }
                    // ... date range ...
                    if overview.dateRangeType == .None {
                        ncenter.fireNotice(Notice(message: .ErrorOverviewDateRangeEmpty))
                        return
                    }
                    else if overview.dateRangeType == .Custom && overview.customDateRange == nil {
                        ncenter.fireNotice(Notice(message: .ErrorOverviewCustomDateRangeInvalid))
                        return
                    }
                    // .. and also selected hashtags.
                    if overview.hashtags.isEmpty {
                        ncenter.fireNotice(Notice(message: .ErrorOverviewHashtagsEmpty))
                        return
                    }
                    // ... dismiss the view controller and ...
                    vc.dismiss() {
                        if !persistent {
                            sself.performSegueWithIdentifier("duration", sender: overview)
                        }
                    }
                    if persistent {
                        // ... try to save the values.
                        DataCenter.defaultCenter().save(overview) { success in
                            // Prompt proper message and reload the list.
                            NotificationCenter.defaultCenter().fireNotice(Notice(message: .SuccessItemDidSave))
                            sself.reloadTableView()

                            if success {
                                sself.gaTrackOverviewEvent("Add", value: 1)
                            }
                        }
                    }
                }
            }
        }

        let modal = EMPartialModalViewController(rootViewController: content!, contentHeight: 416)
        modal.contentViewMaxHeight = UIScreen.mainScreen().bounds.height

        presentViewController(modal, animated: true, completion: nil)
    }

    private func configureTableViewCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        let overview = overviews[indexPath.row]

        var details = ""
        // Stringify selected date range
        if overview.dateRangeType != OverviewDateRange.Custom {
            details += overview.dateRangeType.toString()
        }
        else if let dateRange = overview.customDateRange where dateRange.from != nil && dateRange.to != nil {
            let from = dateFormatter.stringFromDate(dateRange.from!)
            let to = dateFormatter.stringFromDate(dateRange.to!)
            details += from
            if from != to {
                details += " to " + dateFormatter.stringFromDate(dateRange.to!)
            }
        }
        // Put overview hashtags in a string
        let hashtags = overview.hashtags
        if hashtags.isEmpty == false {
            details += "  •  "
            details += "#" + hashtags.last!
            if hashtags.count > 1 {
                let others = hashtags.count - 1
                details += " and \(others) other"
                if others > 1 {
                    details += "s"
                }
            }
        }

        cell.textLabel?.text = overview.title
        cell.detailTextLabel?.text = details

        // Add long press gesture recognizer to cell for activating options.
        if cell.gestureRecognizers == nil {
            let longPressGestureRegognizer = UILongPressGestureRecognizer(target: self, action: Selector("handleLongPress:"))
            longPressGestureRegognizer.minimumPressDuration = 0.75;
            cell.gestureRecognizers = [longPressGestureRegognizer]
        }
    }

    @objc private func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state != UIGestureRecognizerState.Began {
            return
        }

        // Define delete button
        let delete = OptionButtonItem(title: "Delete", needConfirmation: true) { [weak self] in
            if let
                sself = self,
                overview = sself.optionsViewController!.userInfo as? Overview
            {
                sself.optionsViewController?.dismiss(completion: nil)
                DataCenter.defaultCenter().delete(overview) { success in
                    if success {
                        sself.reloadTableView()
                        sself.gaTrackOverviewEvent("Delete", value: -1)
                    }
                }
            }
        }
        delete.titleLabel.font = ApplicationFontStyle.MainTitle.toFont()
        delete.titleLabel.textColor = UIColor.whiteColor()
        delete.backgroundColor = UIColor.jasperColor()

        // Build up option view controller based on created buttons
        let content = OptionsViewController(optionButtons: [delete], cancelCompletion: { [weak self] in
            self?.optionsViewController = nil
            })
        optionsViewController = content

        var frame = content.view.frame
        frame.size.height = content.prefferedHeight
        content.view.frame = frame

        if let
            cell = gestureRecognizer.view as? UITableViewCell,
            indexPath = tableView?.indexPathForCell(cell)
        {
            content.userInfo = overviews[indexPath.row]
        }

        let modal = EMPartialModalViewController(rootViewController: content)
        presentViewController(modal, animated: true, completion: nil)
    }
}

// MARK: - HomeChildViewController Methods
// MARK: -

extension OverviewsViewController: HomeChildViewController {

    func addItemActionDidSetOff() {
        presentOverviewViewController(forOverview: nil)
    }

}

// MARK: - HomeChildViewController Methods
// MARK: -

extension OverviewsViewController: HomeChildViewController {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return overviews.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("OverviewCell") as! UITableViewCell
        cell.layoutMargins = tableView.layoutMargins

        let selectionBackgroundView = UIView()
        selectionBackgroundView.backgroundColor = UIColor.whiteSmokeColor()
        cell.selectedBackgroundView = selectionBackgroundView

        cell.textLabel?.font = ApplicationFontStyle.MainTitle.toFont()
        cell.textLabel?.textColor = UIColor.mineShaftColor()

        cell.detailTextLabel?.font = ApplicationFontStyle.TeenyTiny.toFont()
        cell.detailTextLabel?.textColor = UIColor.aluminumColor()
        
        configureTableViewCell(cell, atIndexPath: indexPath)

        return cell
    }
}

// MARK: - OverviewsViewController Methods
// MARK: -

extension OverviewsViewController {

    private func gaTrackOverviewEvent(action: String, value: Double) {
        let builder = GAIDictionaryBuilder.createEventWithCategory("Overview", action: action, label: nil, value: NSNumber(double: value))
        GAI.sharedInstance().defaultTracker.send(builder.build() as [NSObject: AnyObject])
    }
    
}
