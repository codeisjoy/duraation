//
//  HomeViewController.swift
//  Duraation
//
//  Created by Emad A. on 30/12/2014.
//  Copyright (c) 2014 Emad A. All rights reserved.
//

import UIKit

// MARK: - UINavigationController
// MARK: -

extension UINavigationController {

    public override func childViewControllerForStatusBarStyle() -> UIViewController? {
        return topViewController;
    }
}

// MARK: - HomeChildViewController Protocol
// MARK: -

protocol HomeChildViewController {
    func addItemActionDidSetOff()
}

// MARK: - HomeViewController
// MARK: -

class HomeViewController: UIViewController {

    // MARK: - Private Properties

    private var tmpScrollViewContentOffset: CGPoint = CGPointZero

    // The main scroll view in which view controllers' view is going to be added
    @IBOutlet weak var scrollView: UIScrollView?

    @IBOutlet weak var navigationBarItem: UINavigationItem?
    // The title view for navigation item
    private var titleView: HomeNavigationItemTitleView?

    // A conllection of view controllers added to scroll view
    private var pages = [UIViewController]()

    private var currentPageIndex: Int = 0
    
    // MARK: - Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.whiteColor()
        scrollView?.backgroundColor = UIColor(white: 0.975, alpha: 1)

        // Filling pages collection
        if let stbrd = storyboard {
            pages.append(stbrd.instantiateViewControllerWithIdentifier("activities") as! UIViewController)
            pages.append(stbrd.instantiateViewControllerWithIdentifier("overviews")  as! UIViewController)
        }

        // Enumerate the collection of pages,
        // pulling the title out and add the view into scroll view
        var titles: [String?] = []
        for page: UIViewController in pages {
            titles.append(page.title)
            addChildViewController(page)
            scrollView?.addSubview(page.view)
        }

        titleView = HomeNavigationItemTitleView(titles: titles)
        navigationBarItem?.titleView = titleView
        // Setting the frame of title view
        var frame = navigationBarItem?.titleView?.frame
        frame?.size = CGSize(width: view.bounds.width * 0.7, height: 44)
        titleView?.frame = frame ?? CGRectZero

        // 'Add' bar button at right
        let button = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        button.setImage(UIImage(named: "icon-add"), forState: .Normal)
        button.addTarget(self, action: Selector("navAddButtonDidSetOff"), forControlEvents: .TouchUpInside)
        button.frame = CGRect(origin: CGPointZero, size: button.imageForState(.Normal)?.size ?? CGSizeZero)
        button.adjustsImageWhenHighlighted = false
        
        navigationBarItem?.rightBarButtonItem = UIBarButtonItem(customView: button)

        // Enable swipe back when no navigation bar
        navigationController?.interactivePopGestureRecognizer.delegate = self
        
        let showWalkThrough: AnyObject? = NSUserDefaults().objectForKey(kShowWalkThrough)
        if showWalkThrough?.boolValue != false {
            let walkthrough = WalkThroughViewController()
            presentViewController(walkthrough, animated: true, completion: nil)
        }
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        // For the first time and when this is being called after viewDidLoad the animated property is false.
        // So, at that point a session should be started.
        gaTrackViewController(pages[currentPageIndex].title, session: !animated)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        // To know the scroll view content offset before being disappeared
        tmpScrollViewContentOffset = scrollView?.contentOffset ?? CGPointZero
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if let scrollView = scrollView {
            // Set the content size of scroll view according to its bounds.
            scrollView.contentSize = CGSizeMake(scrollView.bounds.size.width * CGFloat(pages.count), scrollView.bounds.height)
            // Set the last content offset before changing the frame.
            scrollView.setContentOffset(tmpScrollViewContentOffset, animated: false)
            // Set the frame of views inside scroll view.
            let interPageSpace = scrollView.bounds.width - view.bounds.width
            for (index, page) in enumerate(pages) {
                var scrollViewBounds: CGRect = scrollView.bounds
                scrollViewBounds.origin = CGPointZero
                var frame: CGRect = CGRectInset(scrollViewBounds, floor(interPageSpace / 2), 0)
                frame.origin.x += floor(scrollView.bounds.width * CGFloat(index))

                page.view.frame = frame
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Private Methods

    @objc private func navAddButtonDidSetOff() {
        (pages[currentPageIndex] as? HomeChildViewController)?.addItemActionDidSetOff()
    }

}

// MARK: - UIScrollViewDelegate Methods
// MARK: -

extension HomeViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(scrollView: UIScrollView) {
        let portion: CGFloat = scrollView.contentOffset.x / scrollView.bounds.width
        titleView?.scrollPortion = portion
    }

    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index: Int = Int((targetContentOffset.memory.x / scrollView.contentSize.width) * CGFloat(pages.count))
        currentPageIndex = index

        gaTrackViewController(pages[currentPageIndex].title)
    }
}

// MARk: - UIGestureRecognizerDelegate
// MARK: -

extension HomeViewController: UIGestureRecognizerDelegate {

    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return navigationController?.viewControllers.count > 1
    }

}

// MARK: - Google Analytics
// MARK: -

extension HomeViewController {

    private func gaTrackViewController(title: String?, session: Bool = false) {
        let tracker = GAI.sharedInstance().defaultTracker
        let builder  = GAIDictionaryBuilder.createScreenView()
        if session == true {
            builder.set("start", forKey: kGAISessionControl)
        }
        tracker.set(kGAIScreenName, value: title)
        tracker.send(builder.build() as [NSObject: AnyObject])
    }

}