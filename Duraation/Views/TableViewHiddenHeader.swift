//
//  TableViewHiddenHeader.swift
//  Duraation
//
//  Created by Emad A. on 26/04/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

enum TableViewHiddenHeaderContext {
    case Activities, Overviews, None
}

class TableViewHiddenHeader: UIView {

    // MARK: - Properties

    private var context: TableViewHiddenHeaderContext = .None

    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.font = ApplicationFontStyle.TeenyTiny.toFont()
        label.textColor = UIColor.aluminumColor()
        label.numberOfLines = 0
        return label
        }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icon-bullet-gray")
        return imageView
        }()

    // MARK: - Overriden Methods

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(white: 0.90, alpha: 1)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    convenience init(frame: CGRect, context: TableViewHiddenHeaderContext) {
        self.init(frame: frame)

        autoresizingMask = UIViewAutoresizing.FlexibleWidth

        self.context = context
        if context == .Activities {
            textLabel.text = "Amuzing. huh? You can add an activitiy for this.\n\"Tearing up #duraation list! #entertainment\""
            addSubview(textLabel)
            addSubview(imageView)
        }
        else if context == .Overviews {
            textLabel.text = "There is noting worth this effort. Believe me!"
            addSubview(textLabel)
        }

        // The border bottom
        let borderWidth: CGFloat = 1 / UIScreen.mainScreen().scale
        let borderBottom = UIView(frame: CGRect(
            origin: CGPoint(x: CGRectGetMinX(bounds), y: CGRectGetMaxY(bounds) - borderWidth),
            size: CGSize(width: CGRectGetWidth(bounds), height: borderWidth)))
        borderBottom.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        borderBottom.backgroundColor = UIColor.concreteColor()
        addSubview(borderBottom)
    }

    override func drawRect(rect: CGRect) {
        // If it was for activities table draw the vertical timeline dashed line
        if context == .Activities {
            let ctx: CGContextRef = UIGraphicsGetCurrentContext();
            let rct: CGRect = CGRectMake(CGRectGetMinX(rect), CGRectGetMinY(rect), 44, CGRectGetHeight(rect));

            CGContextSetLineDash(ctx, 0, [2.5, 2], 2)
            CGContextSetLineWidth(ctx, 1 / UIScreen.mainScreen().scale)
            CGContextSetStrokeColorWithColor(ctx, UIColor(white: 0.75, alpha: 1).CGColor)
            CGContextMoveToPoint(ctx, CGRectGetMidX(rct), CGRectGetMinY(rct))
            CGContextAddLineToPoint(ctx, CGRectGetMidX(rct), CGRectGetMaxY(rct))
            CGContextStrokePath(ctx)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if context == .Activities {
            textLabel.sizeToFit()
            var frame = textLabel.frame
            frame.origin = CGPoint(x: 44, y: floor(CGRectGetHeight(bounds) / 3))
            textLabel.frame = frame

            imageView.sizeToFit()
            frame = imageView.frame
            frame.origin = CGPoint(x: floor((textLabel.frame.origin.x - frame.width) / 2), y: textLabel.frame.origin.y)
            imageView.frame = frame
        }
        else if context == .Overviews {
            textLabel.sizeToFit()
            var frame = textLabel.frame
            frame.origin = CGPoint(x: floor((CGRectGetWidth(bounds) - frame.width) / 2), y: floor(CGRectGetHeight(bounds) / 3))
            textLabel.frame = frame
        }
    }
}
