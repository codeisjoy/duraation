//
//  EntryFormLabelledCell.swift
//  Duraation
//
//  Created by Emad A. on 18/03/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class EntryFormLabelledCell: EntryFormDefaultCell {

    // MARK: - Public Properties

    private(set) lazy var valueLabel: UITextField = {
        let textField: UITextField = UITextField(frame: self.valueContentView.bounds)
        textField.contentVerticalAlignment = UIControlContentVerticalAlignment.Bottom
        textField.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        textField.font = ApplicationFontStyle.MainTitle.toFont()
        textField.backgroundColor = UIColor.clearColor()
        textField.textColor = UIColor.mineShaftColor()
        textField.borderStyle = UITextBorderStyle.None
        textField.enabled = false

        return textField
        }()

    // MARK: - Overriden Methods

    override func configureCell() {
        super.configureCell()

        selectionStyle = .Default
        selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = UIColor.whiteSmokeColor()

        valueContentView.addSubview(valueLabel)
    }
}
