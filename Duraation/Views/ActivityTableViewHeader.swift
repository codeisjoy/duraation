//
//  ActivityTableViewHeader.swift
//  Duraation
//
//  Created by Emad A. on 24/04/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class ActivityTableViewHeader: UIButton {

    // MARK: - Properties

    var date: NSDate = NSDate() {
        didSet { needsUpdateTitle() }
    }

    // MARK: - Overriden Methods

    override func awakeFromNib() {
        super.awakeFromNib()

        // To set the button settings
        titleLabel?.font = ApplicationFontStyle.LightFifteen.toFont()
        backgroundColor = UIColor.whiteSmokeColor()

        // The border bottom
        let borderWidth: CGFloat = 1 / UIScreen.mainScreen().scale
        let borderBottom = UIView(frame: CGRect(
            origin: CGPoint(x: CGRectGetMinX(bounds), y: CGRectGetMaxY(bounds) - borderWidth),
            size: CGSize(width: CGRectGetWidth(bounds), height: borderWidth)))
        borderBottom.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        borderBottom.backgroundColor = UIColor.concreteColor()
        addSubview(borderBottom)

        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: Selector("needsUpdateTitle"),
            name: UIApplicationWillEnterForegroundNotification,
            object: nil)
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(
            self,
            name: UIApplicationWillEnterForegroundNotification,
            object: nil)
    }

    // MARK: Private Methods

    @objc private func needsUpdateTitle() {
        setTitle(
            DateHelper.formattedStringFromDate(date, unitFlags: .Date, extended: true),
            forState: UIControlState.Normal)
    }
}
