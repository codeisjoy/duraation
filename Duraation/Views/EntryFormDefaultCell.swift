//
//  EntryFormDefaultCell.swift
//  Duraation
//
//  Created by Emad A. on 18/03/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class EntryFormDefaultCell: EntryFormBasicCell {

    // MARK: - Public Properties

    private(set) lazy var captionLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = UIColor.aluminumColor()
        label.font = ApplicationFontStyle.TeenyTiny.toFont()

        return label
        }()

    private(set) lazy var valueContentView: UIView = {
        let view: UIView = UIView()
        view.backgroundColor = UIColor.clearColor()

        return view
        }()

    // MARK: - Public Methods

    convenience init(caption: String) {
        self.init()
        captionLabel.text = caption
    }

    // MARK: - Overriden Methods

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
    }

    override func configureCell() {
        super.configureCell()

        contentView.addSubview(captionLabel)
        contentView.addSubview(valueContentView)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let marginH: CGFloat = layoutMargins.left + layoutMargins.right

        // Setting the caption position
        // When there is no caption set, it should be out of calculation
        if captionLabel.text?.isEmpty == false {
            let fitSize: CGSize = captionLabel.sizeThatFits(CGSizeZero)
            captionLabel.frame = CGRect(
                origin: CGPoint(x: floor(layoutMargins.left), y: floor(layoutMargins.top)),
                size: CGSize(width: floor(contentView.bounds.width - marginH), height: floor(fitSize.height)))
        }
        else {
            captionLabel.frame = CGRectZero
        }

        // Setting the input text field position
        valueContentView.frame = CGRect(
            origin: CGPoint(x: floor(layoutMargins.left), y: floor(CGRectGetMaxY(captionLabel.frame))),
            size: CGSize(width: floor(contentView.bounds.width - marginH),
                         height: preferredHeight - CGRectGetMaxY(captionLabel.frame) - layoutMargins.bottom))
    }

    override func layoutMarginsDidChange() {
        super.layoutMarginsDidChange()
        setNeedsLayout()
    }

}
