//
//  EntryFormTextViewCell.swift
//  Duraation
//
//  Created by Emad A. on 21/03/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class EntryFormTextViewCell: EntryFormDefaultCell {

    // MARK: - Public Properties

    private(set) lazy var textView: UITextView = {
        let textView: UITextView = UITextView(frame: self.valueContentView.bounds)
        textView.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        textView.textContainerInset = self.layoutMargins
        textView.textContainer.lineFragmentPadding = 0
        textView.editable = true

        let paragraph: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraph.firstLineHeadIndent = self.layoutMargins.left
        paragraph.headIndent = paragraph.firstLineHeadIndent

        textView.typingAttributes = [
            NSFontAttributeName: ApplicationFontStyle.MainTitle.toFont(),
            NSForegroundColorAttributeName: UIColor.mineShaftColor()
        ]

        return textView
        }()

    override var preferredHeight: CGFloat {
        get {
            // Preferred height is greater value of text height and standard height.
            let fitSize = textView.sizeThatFits(CGSize(width: textView.bounds.width, height: CGFloat.max))
            return max(fitSize.height, super.preferredHeight)
        }
    }

    // MARK: - Overriden Methods

    override func configureCell() {
        super.configureCell()

        valueContentView.addSubview(textView)

        captionLabel.font = textView.font
        captionLabel.textColor = UIColor.concreteColor()
        contentView.bringSubviewToFront(captionLabel)

        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: Selector("textViewTextDidChange:"),
            name: UITextViewTextDidChangeNotification,
            object: nil)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        // TextView should be fit in the whole content view
        valueContentView.frame = contentView.bounds

        var frame = captionLabel.frame
        frame.origin.y = valueContentView.convertPoint(textView.frame.origin, toView: contentView).y + textView.textContainerInset.top
        frame.size.height = captionLabel.font.lineHeight
        captionLabel.frame = frame
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Set text view as the first responder if cell is selected
        valueContentView.userInteractionEnabled = selected
        if selected && textView.canBecomeFirstResponder() {
            textView.becomeFirstResponder()
        }
        else if !selected && textView.canResignFirstResponder() {
            textView.resignFirstResponder()
        }
    }

    override func layoutMarginsDidChange() {
        super.layoutMarginsDidChange()
        textView.textContainerInset = layoutMargins
    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    // MARK: - Private Methods

    func textViewTextDidChange(notification: NSNotification) {
        if let textView = notification.object as? UITextView where textView == self.textView {
            captionLabel.hidden = count(textView.text) > 0
        }
    }
}