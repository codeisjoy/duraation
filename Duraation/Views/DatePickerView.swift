//
//  DatePickerView.swift
//  Duraation
//
//  Created by Emad A. on 14/02/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

enum DatePickerStyle: Int {
    case Date, Time, DateTime
}

class DatePickerView: UIControl {

    // The names of months, in order
    
    private let months: [String] = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    // The number of days each month has, in order
    private let daysOfMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    // What will be shown for "before noon" and "after noon"
    private let meridiem: [String] = ["AM", "PM"]

    private let startYear: Int = 2015

    // MARK: - Public Properties

    var style: DatePickerStyle {
        didSet {
            initializePickerView()
        }
    }

    var pickedDate: NSDate {
        get { return _pickedDate }
        set { setPickedDate(newValue, animated: false) }
    }

    // MARK: - Private Properties

    private let pickerView: EMPickerView = EMPickerView()

    private var _pickedDate: NSDate = NSDate()

    // MARK: - Overriden Methods

    override init(frame: CGRect) {
        style = .DateTime

        super.init(frame: frame)
        initializePickerView()
    }

    required init(coder aDecoder: NSCoder) {
        style = .DateTime
        
        super.init(coder: aDecoder)
        initializePickerView()
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        addSubview(pickerView)
    }

    // MARK: Public Methods

    func setPickedDate(date: NSDate, animated: Bool) {
        if _pickedDate == date {
            return
        }

        _pickedDate = date

        var units: NSCalendarUnit = .CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay | .CalendarUnitHour | .CalendarUnitMinute
        if style == DatePickerStyle.Date {
            units ^= .CalendarUnitHour | .CalendarUnitMinute
        }
        else if style == DatePickerStyle.Time {
            units ^= .CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay
        }

        let calendar: NSCalendar = NSCalendar.currentCalendar()
        let components: NSDateComponents = calendar.components(units, fromDate: date)

        pickerView.delegate = nil

        if style == .Date {
            selectDate(components: components, section: 0, animated: animated)
        }
        else if style == .Time {
            selectTime(components: components, section: 0, animated: animated)
        }
        else if style == .DateTime {
            selectDate(components: components, section: 0, animated: animated)
            selectTime(components: components, section: 1, animated: animated)
        }

        pickerView.delegate = self
        sendActionsForControlEvents(UIControlEvents.ValueChanged)
    }

    // MARK: Private Methods

    func initializePickerView() {
        pickerView.delegate = self
        pickerView.dataSource = self

        pickerView.frame = bounds;
        pickerView.tintColor = UIColor.summerSkyColor()
        pickerView.autoresizingMask = .FlexibleWidth | .FlexibleHeight
    }

    func selectDate(#components: NSDateComponents, section: Int, animated: Bool) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            [unowned self] in
            self.pickerView.selectRowAtIndex(components.day - 1, component: 0, section: section, animated: animated)
            self.pickerView.selectRowAtIndex(components.month - 1, component: 1, section: section, animated: animated)
            self.pickerView.selectRowAtIndex(components.year - self.startYear, component: 2, section: section, animated: animated)
        }
    }

    func selectTime(#components: NSDateComponents, section: Int, animated: Bool) {
        var h: Int = components.hour
        var ampm: Int = h < 12 ? 0 : 1
        if h == 0 {
            h = 12
        }
        else if h > 12 {
            h -= 12
        }

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            [unowned self] in
            self.pickerView.selectRowAtIndex(h - 1, component: 0, section: section, animated: animated)
            self.pickerView.selectRowAtIndex(components.minute, component: 1, section: section, animated: animated)
            self.pickerView.selectRowAtIndex(ampm, component: 2, section: section, animated: animated)
        }
    }
}

// MARK: - EMPickerViewDataSource Extension
// MARK:

extension DatePickerView: EMPickerViewDataSource {

    func numberOfSectionsInPickerView(pickerView: EMPickerView) -> Int {
        return style == .DateTime ? 2 : 1
    }

    func pickerView(pickerView: EMPickerView, numberOfComponentsInSection section: Int) -> Int {
        return 3
    }

    func pickerView(pickerView: EMPickerView, numberOfRowsInComponent component: Int, section: Int) -> Int {
        if section == 0 {
            if style == .DateTime || style == .Date {
                return numberOfRowsForDateStyle(component)
            }
            else if style == .Time {
                return numberOfRowsForTimeStyle(component)
            }
        }
        else if section == 1 {
            if style == .DateTime {
                return numberOfRowsForTimeStyle(component)
            }
        }

        return 0
    }

    func numberOfRowsForDateStyle(component: Int) -> Int {
        if      component == 0 { return 31 }
        else if component == 1 { return 12 }
        else if component == 2 { return 3  }

        return 0
    }

    func numberOfRowsForTimeStyle(component: Int) -> Int {
        if      component == 0 { return 12 }
        else if component == 1 { return 60 }
        else if component == 2 { return 2  }

        return 0
    }
}

// MARK: - EMPickerViewDelegate Extension
// MARK:

extension DatePickerView: EMPickerViewDelegate {

    func widthFractionForSectionsInPickerView(pickerView: EMPickerView) -> [CGFloat] {
        if style == .DateTime {
            return [0.52, 0.48]
        }

        return [1]
    }

    func pickerView(pickerView: EMPickerView, widthFractionForComponentsInSection section: Int) -> [CGFloat]? {
        if section == 0 {
            if style == .Date {
                return [0.22, 0.48, 0.30]
            }
        }

        return nil
    }

    func pickerView(pickerView: EMPickerView, titleForRow row: Int, component: Int, section: Int) -> String {
        return "-"
    }

    func pickerView(pickerView: EMPickerView, attributtedTitleForRow row: Int, component: Int, section: Int) -> NSAttributedString? {
        var text: String = "-"
        if section == 0 {
            if style == .DateTime || style == .Date {
                text = titleForDateStyle(row, component: component)
            }
            else if style == .Time {
                text = titleForTimeStyle(row, component: component)
            }
        }
        if section == 1 {
            if style == .DateTime {
                text = titleForTimeStyle(row, component: component)
            }
        }

        var title: NSMutableAttributedString = NSMutableAttributedString(string: text)
        title.addAttribute(NSFontAttributeName, value: ApplicationFontStyle.MainTitle.toFont(), range: NSMakeRange(0, title.length))
        if (style == .DateTime || style == .Date) && section == 0 && component == 1 {
            let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .Left
            title.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, title.length))
        }

        return title
    }

    func pickerView(pickerView: EMPickerView, didSelectRowAtIndex row: Int, component: Int, section: Int) {
        var components = NSDateComponents()
        components.timeZone = NSTimeZone.systemTimeZone()

        var updated = true
        if style == DatePickerStyle.Date {
            updated = updated && updateDateComponentsWithDate(section: 0, components: &components)
        }
        else if style == DatePickerStyle.Time {
            updated = updated && updateDateComponentsWithTime(section: 0, components: &components)
        }
        else if style == DatePickerStyle.DateTime {
            updated = updated && updateDateComponentsWithDate(section: 0, components: &components)
            updated = updated && updateDateComponentsWithTime(section: 1, components: &components)
        }

        if updated == false {
            return
        }

        let calendar = NSCalendar.currentCalendar()
        if let date = calendar.dateFromComponents(components) {
            let units: NSCalendarUnit = .CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay | .CalendarUnitHour | .CalendarUnitMinute
            let compared: NSDateComponents = calendar.components(units, fromDate: date, toDate: pickedDate, options: NSCalendarOptions.allZeros)
            if compared.year | compared.month | compared.day | compared.hour | compared.minute != 0 {
                _pickedDate = date
                sendActionsForControlEvents(UIControlEvents.ValueChanged)
            }
        }
    }

    func titleForDateStyle(row: Int, component: Int) -> String {
        if component == 0 { return digitalise(row + 1) }
        else if component == 1 {
            let month = months[row]
            if style == .DateTime {
                return month.substringToIndex(advance(month.startIndex, 3))
            }
            else {
                return month
            }
        }
        else if component == 2 { return "\(startYear + row)" }

        return "-"
    }

    func titleForTimeStyle(row: Int, component: Int) -> String {
        if      component == 0 { return digitalise(row + 1) }
        else if component == 1 { return digitalise(row) }
        else if component == 2 { return meridiem[row] }

        return "-"
    }

    func digitalise(number: Int) -> String {
        if number > 9 { return "\(number)" }
        else { return "0\(number)" }
    }

    func updateDateComponentsWithDate(#section: Int, inout components: NSDateComponents) -> Bool {
        let d: Int = pickerView.selectedRow(component: 0, section: section)
        let m: Int = pickerView.selectedRow(component: 1, section: section)
        let y: Int = pickerView.selectedRow(component: 2, section: section)

        if d != NSNotFound && m != NSNotFound && y != NSNotFound {
            if d + 1 > daysOfMonth[m] {
                pickerView.selectRowAtIndex(daysOfMonth[m] - 1, component: 0, section: section, animated: true)
                return false
            }

            components.day = d + 1
            components.month = m + 1
            components.year = y + startYear
            return true
        }

        return false
    }

    func updateDateComponentsWithTime(#section: Int, inout components: NSDateComponents) -> Bool {
        let h: Int = pickerView.selectedRow(component: 0, section: section)
        let m: Int = pickerView.selectedRow(component: 1, section: section)
        let ampm: Int  = pickerView.selectedRow(component: 2, section: section)

        if h != NSNotFound && m != NSNotFound && ampm != NSNotFound {
            components.hour = ((h + 1 < 12) ? h + 1 : 0) + ampm * 12
            components.minute = m

            return true
        }

        return false
    }
}