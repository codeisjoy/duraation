//
//  EntryFormDatePickerCell.swift
//  Duraation
//
//  Created by Emad A. on 18/03/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class EntryFormDatePickerCell: EntryFormLabelledCell {

    // MARK: - Public Properties

    let datePicker: DatePickerView = DatePickerView()

    override var preferredHeight: CGFloat {
        get { return selected ? super.preferredHeight + datePicker.bounds.height : super.preferredHeight }
    }

    // MARK: - Private Methods

    private let hairLineHeight: CGFloat = 1 / UIScreen.mainScreen().scale
    private let fixedSeparator: UIView = UIView()

    // MARK: - Overriden Methods

    override func configureCell() {
        super.configureCell()

        fixedSeparator.backgroundColor = UIColor.concreteColor()
        addSubview(fixedSeparator)

        datePicker.addTarget(self, action: Selector("datePickerViewValueDidChange:"), forControlEvents: UIControlEvents.ValueChanged)
        contentView.addSubview(datePicker)
    }

    override func layoutSubviews() {
        super.layoutSubviews();

        let marginH: CGFloat = layoutMargins.left + layoutMargins.right

        // Setting the value field position
        valueContentView.frame = CGRect(
            origin: CGPoint(x: floor(layoutMargins.left), y: floor(CGRectGetMaxY(captionLabel.frame))),
            size: CGSize(width: floor(contentView.bounds.width - marginH),
                         height: super.preferredHeight - CGRectGetMaxY(captionLabel.frame) - layoutMargins.bottom))

        // Setting the date picker position
        datePicker.frame = CGRect(
            origin: CGPoint(x: floor(layoutMargins.left), y: super.preferredHeight),
            size: CGSize(width: floor(contentView.bounds.width - marginH), height: 180))

        // Setting the fixed separator view position above the date picker view
        fixedSeparator.frame = CGRect(
            origin: CGPoint(x: layoutMargins.left, y: CGRectGetMinY(datePicker.frame) - hairLineHeight),
            size: CGSize(width: bounds.width - layoutMargins.left, height: hairLineHeight))

        // Setting selected background view frame to fit the top portion of cell, not date picker view
        var frame: CGRect = selectedBackgroundView.frame
        frame.size.height = super.preferredHeight
        selectedBackgroundView.frame = frame

        // Keep accessory view in a place to avoid moving when the date picker is open
        if let accessory = accessoryView {
            var frame = accessory.frame
            frame.origin.y = (CGRectGetMinY(datePicker.frame) - CGRectGetHeight(frame)) / 2
            accessory.frame = frame
        }
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        fixedSeparator.backgroundColor = UIColor.concreteColor()
    }

    override func setHighlighted(highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        fixedSeparator.backgroundColor = UIColor.concreteColor()
    }

    // MARK: - Private Method

    func datePickerViewValueDidChange(sender: DatePickerView) {
        var units: FormattedDateUnit = .Date | .Time
        if sender.style == .Time {
            units ^= .Date
        }
        else if sender.style == .Date {
            units ^= .Time
        }

        valueLabel.text = DateHelper.formattedStringFromDate(sender.pickedDate, unitFlags: units)
    }
}
