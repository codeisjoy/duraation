//
//  DurationTitleView.swift
//  Duraation
//
//  Created by Emad A. on 24/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class DurationNavigationItemTitleView: UIView {

    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        label.font = ApplicationFontStyle.NavTitle.toFont()
        return label
        }()

    private(set) lazy var promptLabel: UILabel = {
        let label = UILabel()
        label.alpha = 0.7
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        label.font = ApplicationFontStyle.LightFifteen.toFont()
        return label
        }()

    // MARK: - Overriden Methods

    override init(frame: CGRect) {
        super.init(frame: frame)

        clipsToBounds = true
        backgroundColor = UIColor.clearColor()

        addSubview(titleLabel)
        addSubview(promptLabel)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        titleLabel.center = CGPoint(x: CGRectGetMidX(bounds), y: CGRectGetMidY(bounds))
        if promptLabel.hidden == false {
            promptLabel.center = CGPoint(x: CGRectGetMidX(bounds), y: CGRectGetMaxY(bounds) + promptLabel.bounds.height / 2)
        }
    }

    // MARK: -

    func setTitle(title: String, prompt: String?) {
        titleLabel.text = title
        titleLabel.sizeToFit()

        promptLabel.text = prompt
        promptLabel.hidden = (prompt == nil)
        promptLabel.sizeToFit()
    }

}
