//
//  HomeTitleView.swift
//  Duraation
//
//  Created by Emad A. on 31/12/2014.
//  Copyright (c) 2014 Emad A. All rights reserved.
//

import UIKit

// MARK: - HomeNavigationBar
// MARK: -

class HomeNavigationBar: UINavigationBar {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.shadowImage = self.greenShadowImage()
        self.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
    }

    func greenShadowImage() -> UIImage {
        let rect: CGRect = CGRect(origin: CGPoint.zeroPoint, size: CGSize(width: self.bounds.size.width, height: 2))

        UIGraphicsBeginImageContext(rect.size)
        let ctx: CGContextRef = UIGraphicsGetCurrentContext()

        CGContextSetFillColorWithColor(ctx, UIColor.appleGreenColor().CGColor)
        CGContextFillRect(ctx, rect)

        let shadowImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return shadowImage
    }
    
}

// MARK: - HomeTitleView
// MARK: -

class HomeNavigationItemTitleView: UIView {

    // MARK: - Constancts

    private let titleOpacityFactor: CGFloat = 2.78

    // MARK: - Public Properties

    var scrollPortion: CGFloat = 0 {
        didSet {
            let offset: CGPoint = CGPoint(x: scrollPortion * scrollView.bounds.width, y: 0)
            scrollView.setContentOffset(offset, animated: false)
        }
    }

    // MARK: - Private Properties

    private var labels: [UILabel] = []
    private var pageControl: FXPageControl = FXPageControl()
    private var scrollView: UIScrollView = UIScrollView()

    // Mark: - Overriden Merhods
    
    convenience init(titles: [String?]) {
        self.init()

        clipsToBounds = true
        userInteractionEnabled = false

        // Setting up the title scroll view
        scrollView.delegate = self
        scrollView.pagingEnabled = true
        scrollView.clipsToBounds = false
        scrollView.showsHorizontalScrollIndicator = false

        // Adding labels into scroll view for each given title string
        for title in titles {
            let label: UILabel = UILabel(frame: frame)
            label.textAlignment = NSTextAlignment.Center
            label.font = ApplicationFontStyle.NavTitle.toFont()
            label.text = title

            scrollView.addSubview(label)
            labels.append(label)
        }

        addSubview(scrollView)

        // Setting up the page control
        pageControl.dotSize = 4
        pageControl.dotSpacing = 4
        pageControl.opaque = false
        pageControl.numberOfPages = labels.count
        pageControl.dotColor = UIColor(white: 0, alpha: 0.18)
        pageControl.selectedDotColor = UIColor(white: 0, alpha: 0.40)

        addSubview(pageControl)

        // Setting up a mask layer for fade out both sides of the view
        let outerColor: UIColor = UIColor(white: 1, alpha: 0)
        let innerColor: UIColor = UIColor(white: 1, alpha: 1)

        let maskLayer: CAGradientLayer = CAGradientLayer()
        maskLayer.bounds = bounds;
        maskLayer.startPoint = CGPointMake(0, 0);
        maskLayer.endPoint = CGPointMake(1, 0);
        maskLayer.anchorPoint = CGPointMake(0, 0);
        maskLayer.colors = [outerColor.CGColor, innerColor.CGColor, innerColor.CGColor, outerColor.CGColor]
        maskLayer.locations = [0, 0.1, 0.9, 1]

        layer.mask = maskLayer
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layer.mask.bounds = bounds;

        // Setting the page control frame
        pageControl.frame = CGRectMake(0, bounds.height - pageControl.dotSize * 3, bounds.width, pageControl.dotSize);

        // Setting the title scroll view frame and content size
        scrollView.frame = CGRectInset(bounds, bounds.width * 0.14, 0)
        scrollView.frame.size.height -= pageControl.dotSize * 3 / 2

        scrollView.contentSize = CGSize(
            width: scrollView.bounds.width * CGFloat(labels.count),
            height: scrollView.bounds.height)

        // Setting the labels frame inside scroll view
        for (index, label) in enumerate(labels) {
            var frame: CGRect = scrollView.bounds
            frame.origin.x += frame.size.width * CGFloat(index)

            label.frame = frame
        }
    }
}

// MARK: - UIScrollViewDelegate Methods
// MARK: -

extension HomeNavigationItemTitleView: UIScrollViewDelegate {

    func scrollViewDidScroll(scrollView: UIScrollView) {
        let range: CGFloat = scrollView.contentOffset.x / (scrollView.contentSize.width / CGFloat(labels.count))
        let fraction: CGFloat = min(max(ceil(range) - range, 0), 1)

        // Setting page control current page
        pageControl.currentPage = Int(round(range))

        // Setting the opacity of title label,
        // for one it the left side
        let leftIndex: Int = Int(floor(range))
        if leftIndex >= 0 {
            let label: UILabel = labels[leftIndex]
            label.alpha = pow(fraction, titleOpacityFactor)
        }

        // Setting the opacity of title label,
        // for one it the right side
        let rightIndex: Int = Int(ceil(range))
        if rightIndex < labels.count {
            let label: UILabel = labels[rightIndex]
            label.alpha = 1 - pow(fraction, 1 / titleOpacityFactor)
        }
    }
}