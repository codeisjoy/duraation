//
//  EntryFormTextFieldCell.swift
//  Duraation
//
//  Created by Emad A. on 18/03/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class EntryFormTextFieldCell: EntryFormDefaultCell {

    // MARK: - Public Properties

    private(set) lazy var textField: UITextField = {
        let textField: UITextField = UITextField(frame: self.valueContentView.bounds)
        textField.contentVerticalAlignment = UIControlContentVerticalAlignment.Bottom
        textField.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        textField.font = ApplicationFontStyle.MainTitle.toFont()
        textField.backgroundColor = UIColor.clearColor()
        textField.textColor = UIColor.mineShaftColor()
        textField.borderStyle = UITextBorderStyle.None

        return textField
        }()

    // MARK: - Overriden Methods

    override func configureCell() {
        super.configureCell()
        valueContentView.addSubview(textField)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        valueContentView.userInteractionEnabled = selected
        if selected && textField.canBecomeFirstResponder() {
            textField.becomeFirstResponder()
        }
        else if !selected && textField.canResignFirstResponder() {
            textField.resignFirstResponder()
        }
    }
}
