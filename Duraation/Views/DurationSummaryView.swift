//
//  DurationSummaryView.swift
//  Duraation
//
//  Created by Emad A. on 23/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit
import Darwin

class DurationSummaryView: UIView {

    private let contentEdgeInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)

    // MARK: - Private Properties

    private var loading: WaitingIndicatorView?

    private lazy var durationLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        label.font = ApplicationFontStyle.HugeLight.toFont()
        return label
        }()

    private lazy var separatorHairlineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.2)
        return view
        }()

    private lazy var dateRangeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        label.font = ApplicationFontStyle.LightFifteen.toFont()
        return label
        }()

    private lazy var mediatorLabel: UILabel = {
        let label = UILabel()
        label.text = "on activities contain"
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor(white: 1, alpha: 0.7)
        label.font = ApplicationFontStyle.LightFifteen.toFont()
        return label
        }()

    private lazy var hashtagsButton: UIButton = {
        let button = UIButton.buttonWithType(UIButtonType.System) as! UIButton
        button.addTarget(self, action: Selector("showAllHashtags"), forControlEvents: .TouchUpInside)
        button.contentEdgeInsets = UIEdgeInsets(top: 1, left: 5, bottom: 0.1, right: 5)
        button.titleLabel?.font = ApplicationFontStyle.SmallHeavy.toFont()
        return button
        }()

    private var presentAllHashtags: (Void -> Void)?

    // MARK: - Overriden Methods

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = UIColor.appleGreenColor()

        updateDurationLabel(hours: 0, minutes: 0)
        durationLabel.alpha = 0

        loading = WaitingIndicatorView(frame: CGRect(
            origin: CGPointZero,
            size: CGSize(width: 20, height: 20)))
        addSubview(loading!)

        addSubview(durationLabel)
        addSubview(separatorHairlineView)

        addSubview(dateRangeLabel)
        addSubview(mediatorLabel)
        addSubview(hashtagsButton)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let rect = UIEdgeInsetsInsetRect(bounds, contentEdgeInset)

        // Positioning the duration label
        var size = durationLabel.sizeThatFits(rect.size)
        durationLabel.frame = CGRect(
            origin: CGPoint(x: CGRectGetMinX(rect), y: CGRectGetMinY(rect)),
            size: CGSize(width: rect.width, height: size.height))

        loading?.center = CGPoint(x: CGRectGetMidX(durationLabel.frame), y: CGRectGetMidY(durationLabel.frame))

        // Positioniong the hashtags label, from the bottom
        size = hashtagsButton.sizeThatFits(rect.size)
        hashtagsButton.frame = CGRect(
            origin: CGPoint(x: CGRectGetMidX(rect) - size.width / 2, y: CGRectGetMaxY(rect) - size.height),
            size: size)

        // Positioning the label between hashtags and date range label, with static text
        size = mediatorLabel.sizeThatFits(rect.size)
        mediatorLabel.frame = CGRect(
            origin: CGPoint(x: CGRectGetMinX(rect), y: CGRectGetMinY(hashtagsButton.frame) - size.height),
            size: CGSize(width: rect.width, height: size.height))

        // Positioning the date range label
        size = dateRangeLabel.sizeThatFits(rect.size)
        dateRangeLabel.frame = CGRect(
            origin: CGPoint(x: CGRectGetMinX(rect), y: CGRectGetMinY(mediatorLabel.frame) - size.height),
            size: CGSize(width: rect.width, height: size.height))

        // Positioning the separator line between date range and duration label
        size = CGSize(width: bounds.width * 0.6, height: 1 / UIScreen.mainScreen().scale)
        separatorHairlineView.frame = CGRect(
            origin: CGPoint(x: (bounds.width - size.width) / 2, y: CGRectGetMinY(dateRangeLabel.frame) - contentEdgeInset.bottom),
            size: size)
    }

    // MARK: - Public Methods

    func updateDurationLabel(#hours: UInt, minutes: UInt) {
        durationLabel.alpha = 0

        var text = ""

        var hrstxt = "HR "
        let mintxt = "MIN"

        // Hours
        if hours > 1 {
            hrstxt = "HRS "
        }
        text += "\(hours)" + hrstxt

        // Minutes
        if minutes < 10 {
            text += "0"
        }
        text += "\(minutes)" + mintxt

        // Formatting the duration text
        let formattedText = NSMutableAttributedString(string: text)
        let nstext: NSString = NSString(string: text)

        let hrstxtRange = nstext.rangeOfString(hrstxt)
        formattedText.addAttribute(NSFontAttributeName, value: ApplicationFontStyle.SmallHeavy.toFont(), range: hrstxtRange)
        formattedText.addAttribute(NSForegroundColorAttributeName, value: UIColor(white: 0, alpha: 0.2), range: hrstxtRange)

        let mintxtRange = nstext.rangeOfString(mintxt)
        formattedText.addAttribute(NSFontAttributeName, value: ApplicationFontStyle.SmallHeavy.toFont(), range: mintxtRange)
        formattedText.addAttribute(NSForegroundColorAttributeName, value: UIColor(white: 0, alpha: 0.2), range: mintxtRange)

        durationLabel.attributedText = formattedText
        setNeedsLayout()

        durationLabel.alpha = 0
        loading?.removeFromSuperview()
        loading = nil

        UIView.animateWithDuration(0.25, animations: { self.durationLabel.alpha = 1 })
    }

    func updateHashtagsLabel(#hashtags: [String], showAll: Void -> Void) {
        presentAllHashtags = showAll
        hashtagsButton.userInteractionEnabled = hashtags.count > 2

        var text = ""
        var thinTextRange: NSRange?
        // A mutable copy of hashtags which is sorted alphabetically as well
        var sortedHashtags = hashtags
        if hashtags.count == 1 {
            text = "#" + hashtags.first!
        }
        else if hashtags.count > 1 {
            sortedHashtags.sort({ $0 <= $1 })
            for index in 0...1 {
                text += "#" + sortedHashtags[index] + " "
            }
            if hashtags.count > 2 {
                thinTextRange = NSMakeRange(NSString(string: text).length, 3)
                let others = hashtags.count - 2
                text += "and \(others) other"
                if others > 1 {
                    text += "s"
                }
            }
        }

        text = text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())

        let textNormal = NSMutableAttributedString(string: text)
        textNormal.addAttribute(NSForegroundColorAttributeName, value: UIColor.whiteColor(), range: NSMakeRange(0, textNormal.length))
        if let range = thinTextRange {
            textNormal.addAttribute(NSFontAttributeName, value: ApplicationFontStyle.LightFifteen.toFont(), range: range)
        }

        hashtagsButton.setAttributedTitle(textNormal, forState: .Normal)
        setNeedsLayout()
    }

    func updateDateRangeLabel(#string: String, range: (from: NSDate, to: NSDate)?) {
        if range == nil {
            dateRangeLabel.text = string
            return
        }

        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd MMM yyyy"

        // Create the date range string
        let conjunction = "  to  "
        let to = formatter.stringFromDate(range!.to)
        var text = formatter.stringFromDate(range!.from)
        if text != to {
            text += conjunction + to
        }

        // Format the date range string
        let formattedText = NSMutableAttributedString(string: text)
        let conjunctionRange = NSString(string: text).rangeOfString(conjunction)
        formattedText.addAttribute(NSForegroundColorAttributeName, value: UIColor(white: 1, alpha: 0.7), range: conjunctionRange)

        dateRangeLabel.attributedText = formattedText
    }

    // MARK: - Private Methods

    @objc private func showAllHashtags() {
        presentAllHashtags?()
    }
}

// MARK: - WaitingIndicatorView
// MARK: -

class WaitingIndicatorView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = UIColor.clearColor()

        let rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation.toValue = 2 * M_PI
        rotation.repeatCount = HUGE
        rotation.speed = 0.25
        layer.addAnimation(rotation, forKey: "rotation")
    }

    deinit {
        layer.removeAllAnimations()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawRect(rect: CGRect) {
        let ctx = UIGraphicsGetCurrentContext()
        let r = UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2))

        CGContextSetStrokeColorWithColor(ctx, UIColor.whiteColor().CGColor)
        CGContextAddEllipseInRect(ctx, r)
        CGContextStrokePath(ctx)

        CGContextSetFillColorWithColor(ctx, UIColor.appleGreenColor().CGColor)
        CGContextFillRect(ctx, CGRect(origin: CGPoint(x: CGRectGetMidX(r) - 5, y: CGRectGetMinY(r) - 1), size: CGSize(width: 10, height: 5)))
    }

}
