//
//  EmptyTableViewLabel.swift
//  
//
//  Created by Emad A. on 20/06/2015.
//
//

import UIKit

class EmptyTableViewLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)

        numberOfLines = 0
        textAlignment = .Center
        textColor = UIColor(white: 0, alpha: 0.28)
        font = UIFont(name: "Avenir-Medium", size: 14)!
        autoresizingMask = .FlexibleWidth
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
