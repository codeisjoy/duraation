//
//  ActivityTableViewCell.swift
//  Duraation
//
//  Created by Emad A. on 16/01/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class ActivityTableViewCell: UITableViewCell {

    // MARK: - Properties

    @IBOutlet var durationLabel: UILabel?
    @IBOutlet var timeLabel: UILabel?
    @IBOutlet var noteLabel: UILabel?

    // MARK: Overriden Methods

    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    override func drawRect(rect: CGRect) {
        let ctx: CGContextRef = UIGraphicsGetCurrentContext();
        let rct: CGRect = CGRectMake(CGRectGetMinX(rect), CGRectGetMinY(rect), durationLabel?.frame.origin.x ?? 0, CGRectGetHeight(rect));

        CGContextSetLineDash(ctx, 0, [2.5, 2], 2);
        CGContextSetStrokeColorWithColor(ctx, UIColor.concreteColor().CGColor)
        CGContextMoveToPoint(ctx, CGRectGetMidX(rct), CGRectGetMinY(rct));
        CGContextAddLineToPoint(ctx, CGRectGetMidX(rct), CGRectGetMaxY(rct));
        CGContextStrokePath(ctx)
    }

    // MARK: Public Methods

    func updateViewWithActivity(activity: Activity, reference: NSDate = NSDate(timeIntervalSince1970: 0)) {
        let calendar: NSCalendar = NSCalendar.currentCalendar()

        // Calculate the duration and create the string value.
        var dUnits: NSCalendarUnit = .CalendarUnitHour | .CalendarUnitMinute
        var dComponents = calendar.components(dUnits, fromDate: activity.start, toDate: activity.end, options: .allZeros)

        var durationString = ""
        if dComponents.hour > 48 {
            dUnits |= .CalendarUnitDay
            dComponents = calendar.components(dUnits, fromDate: activity.start, toDate: activity.end, options: .allZeros)
            durationString += "\(dComponents.day)d "
        }
        if dComponents.hour > 0 {
            durationString += "\(dComponents.hour)h "
        }
        if dComponents.minute > 0 {
            if dComponents.minute < 10 {
                durationString += "0"
            }
            durationString += "\(dComponents.minute)m "
        }

        // Calculate the start and end date
        let dateFormatter = NSDateFormatter()
        dateFormatter.AMSymbol = "AM"
        dateFormatter.PMSymbol = "PM"

        let tUnits: NSCalendarUnit = .CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay
        let referenceComponents = calendar.components(tUnits, fromDate: reference)
        let startComponents = calendar.components(tUnits, fromDate: activity.start)
        let endComponents = calendar.components(tUnits, fromDate: activity.end)

        // Time string depends of reference date and start and end date may vary.
        var timeString = ""

        // If start and end date are in an exact day ...
        if startComponents == endComponents {
            // so no need to have the date part. Time is enough.
            dateFormatter.dateFormat = "hh:mm a"
            // If start and reference date are not in the same day, date of start should be printed out.
            if referenceComponents != startComponents {
                dateFormatter.dateFormat = "dd MMM yyyy, \(dateFormatter.dateFormat)"
            }
            timeString += dateFormatter.stringFromDate(activity.start)

            // For end, just add the time.
            dateFormatter.dateFormat = " - hh:mm a"
            timeString += dateFormatter.stringFromDate(activity.end)
        }
        // If start and end date are not in the same day ...
        else {
            // the date and time parts of each will be printed out.
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
            timeString += dateFormatter.stringFromDate(activity.start)
            timeString += " - \(dateFormatter.stringFromDate(activity.end))"
        }

        // Bringing calculated value into view
        durationLabel?.text = durationString
        timeLabel?.text = timeString
        noteLabel?.text = activity.note
    }

    // MARK: Private Methods

    func configureCell() {
        durationLabel?.textColor = UIColor.mineShaftColor()
        durationLabel?.font = ApplicationFontStyle.MainTitle.toFont()

        timeLabel?.textColor = UIColor.aluminumColor()
        timeLabel?.font = ApplicationFontStyle.TeenyTiny.toFont()

        noteLabel?.textColor = UIColor.mineShaftColor()
        noteLabel?.font = ApplicationFontStyle.Body.toFont()
    }

}