//
//  EntryFormSelectionalCell.swift
//  Duraation
//
//  Created by Emad A. on 8/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class EntryFormSelectionalCell: EntryFormBasicCell {

    override var preferredHeight: CGFloat {
        return 54
    }

    var optionSelected: Bool = false {
        didSet { setOptionSelectedAcceessoryView() }
    }

    private var selectedOptionView = UIImageView(image: UIImage(named: "selected-option"))
    private var unselectedOptionView = UIImageView(image: UIImage(named: "unselected-option"))

    // MARK: - Overriden Methods

    override func configureCell() {
        super.configureCell()

        selectionStyle = .Default
        selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = UIColor.whiteSmokeColor()
        setOptionSelectedAcceessoryView()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected == true {
            optionSelected = !optionSelected
        }
    }

    private func setOptionSelectedAcceessoryView() {
        accessoryView = optionSelected ? selectedOptionView : unselectedOptionView
    }
}
