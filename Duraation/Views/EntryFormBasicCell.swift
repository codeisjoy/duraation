//
//  EntryFormBasicCell.swift
//  Duraation
//
//  Created by Emad A. on 8/05/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class EntryFormBasicCell: UITableViewCell {

    var preferredHeight: CGFloat {
        return 68
    }

    // MARK: - Private Properties

    private let hairLineHeight: CGFloat = 1 / UIScreen.mainScreen().scale

    private lazy var separator: UIView = {
        let view: UIView = UIView()
        view.backgroundColor = UIColor.concreteColor()
        return view
        }()

    // Overriden Methods

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        // Setting the separator view position
        separator.frame = CGRect(
            origin: CGPoint(x: layoutMargins.left, y: CGRectGetMaxY(bounds) - hairLineHeight),
            size: CGSize(width: bounds.width - layoutMargins.left, height: hairLineHeight))
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        separator.backgroundColor = UIColor.concreteColor()

        // The cell selection highlight will blink at selection
        if let backgroundView = selectedBackgroundView {
            if selected == false {
                backgroundView.alpha = 1
            }
            else {
                backgroundView.alpha = 0
            }
        }
    }

    override func setHighlighted(highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        separator.backgroundColor = UIColor.concreteColor()
    }

    // MARK: -
    
    internal func configureCell() {
        clipsToBounds = true
        selectionStyle = .None

        addSubview(separator)
    }

}
