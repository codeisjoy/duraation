//
//  ModalNavigationView.swift
//  Duraation
//
//  Created by Emad A. on 24/02/2015.
//  Copyright (c) 2015 Emad A. All rights reserved.
//

import UIKit

class ModalNavigationView: UIView {

    // MARK: - Public Properties

    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.whiteColor()
        label.font = ApplicationFontStyle.MainTitle.toFont()
        return label
        }()

    private(set) lazy var doneButton: UIButton = {
        let button = UIButton.buttonWithType(.Custom) as! UIButton
        button.titleLabel?.font = ApplicationFontStyle.LightFifteen.toFont()
        button.setTitle("DONE", forState: UIControlState.Normal)
        button.setTitleColor(UIColor(white: 1, alpha: 0.7), forState: UIControlState.Normal)
        button.setTitleColor(UIColor(white: 1, alpha: 1.0), forState: UIControlState.Highlighted)
        return button
        }()

    private(set) lazy var backButton: UIButton = {
        let button = UIButton.buttonWithType(.Custom) as! UIButton
        button.setImage(UIImage(named: "icon-back"), forState: .Normal)
        button.setImage(UIImage(named: "icon-back-highlighted"), forState: .Highlighted)
        return button
        }()

    var backButtonHidden: Bool = true {
        didSet { initialize() }
    }
    var doneButtonHidden: Bool = true {
        didSet { initialize() }
    }

    // MARK - Private Properties

    // Overriden Methods

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        addSubview(titleLabel)
        initialize()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if titleLabel.text?.isEmpty == false {
            titleLabel.sizeToFit()
            titleLabel.center = CGPoint(x: CGRectGetMidX(bounds), y: CGRectGetMidY(bounds))
        }

        if doneButtonHidden == false {
            doneButton.sizeToFit()
            doneButton.frame = CGRect(
                origin: CGPoint(x: CGRectGetMaxX(bounds) - CGRectGetWidth(doneButton.bounds) - 40, y: CGRectGetMinY(bounds)),
                size: CGSize(width: CGRectGetWidth(doneButton.bounds) + 40, height: CGRectGetHeight(bounds)))
        }

        if backButtonHidden == false {
            backButton.sizeToFit()
            backButton.frame = CGRect(
                origin: CGPoint(x: CGRectGetMinX(bounds), y: CGRectGetMinY(bounds)),
                size: CGSize(width: CGRectGetWidth(backButton.bounds) + 40, height: CGRectGetHeight(bounds)))
        }
    }

    // MARK: - Private Methods

    func initialize() {
        doneButtonHidden == false ?
            addSubview(doneButton) :
            doneButton.removeFromSuperview()

        backButtonHidden == false ?
            addSubview(backButton) :
            backButton.removeFromSuperview()
    }
}
