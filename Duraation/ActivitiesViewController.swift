//
//  ActivitiesViewController.swift
//  Duraation
//
//  Created by Emad A. on 30/12/2014.
//  Copyright (c) 2014 Emad A. All rights reserved.
//

import UIKit

class ActivitiesViewController: UIViewController {

    private let activityNoteLengthLimit: Int = 109

    private let tableViewCellId = "ActivityCell"

    // MARK: - Public Properties

    // A button that bring the date picker on screen to filter table data
    @IBOutlet weak var dateButton: ActivityTableViewHeader!
    // The main table view
    @IBOutlet weak var tableView: UITableView?

    // MARK: - Private Properties

    // Contains what table view should list
    private var activities = [Activity]() {
        didSet {
            if activities.count == 0 {
                if let tableView = tableView {
                    emptyListLabel.frame = CGRect(
                        origin: CGPoint(x: CGRectGetMinX(tableView.bounds), y: CGRectGetMinY(tableView.bounds)),
                        size: CGSize(width: CGRectGetWidth(tableView.bounds), height: 140))
                }
                tableView?.addSubview(emptyListLabel)
            }
            else {
                emptyListLabel.removeFromSuperview()
            }
        }
    }

    // An instance of table view cell being used to have dynamic cell height
    private lazy var prototypeCell: ActivityTableViewCell = {
        [unowned self] in
        return self.tableView?.dequeueReusableCellWithIdentifier(self.tableViewCellId) as! ActivityTableViewCell
        }()

    // The outer border of view appears when parent is scrolling
    private lazy var extendedBorderView: UIView = {
        let view: UIView = UIView();
        view.backgroundColor = UIColor(white: 0.85, alpha: 1)
        return view
        }()

    private let emptyListLabel = EmptyTableViewLabel(frame: CGRectZero)

    // Contains a reference to option view controller and related activity
    private var optionsViewController: OptionsViewController?

    // MARK: - Overriden Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setting up the date button, at the top of the table view
        dateButton.date = NSDate()
        dateButton.addTarget(
            self,
            action: Selector("presentDatePickerViewController"),
            forControlEvents: UIControlEvents.TouchUpInside)

        // Get current activities for curent date.
        activities = DataCenter.defaultCenter().getActivities(forDate: dateButton.date)
        // Configuring table view.
        tableView?.estimatedRowHeight = 100
        tableView?.tableFooterView = UIView()
        tableView?.separatorInset = UIEdgeInsetsZero
        tableView?.layoutMargins = UIEdgeInsetsZero
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.separatorColor = UIColor.concreteColor()
        tableView?.registerNib(
            UINib(nibName: "ActivityTableViewCell", bundle: NSBundle.mainBundle()),
            forCellReuseIdentifier: tableViewCellId)

        if let tableView = tableView {
            let hiddenHeader = TableViewHiddenHeader(frame: CGRect(
                origin: CGPoint(x: CGRectGetMinX(tableView.bounds), y: CGRectGetHeight(tableView.bounds) * -1),
                size: CGSize(width: CGRectGetWidth(tableView.bounds), height: CGRectGetHeight(tableView.bounds))),
                context: TableViewHiddenHeaderContext.Activities)
            tableView.addSubview(hiddenHeader)

            emptyListLabel.text = "No activitiy for this day\nChange the date or add one"
        }

        // Add extented border view beneath all views.
        view.insertSubview(extendedBorderView, atIndex: 0);
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        extendedBorderView.frame = CGRectInset(view.bounds, -1 / UIScreen.mainScreen().scale, 0);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Private Methods

    private func reloadTableView() {
        // Get the activities for selected date.
        activities = DataCenter.defaultCenter().getActivities(forDate: dateButton.date)
        // Reload table view with new data.
        tableView?.setContentOffset(CGPointZero, animated: false)
        tableView?.reloadData()
    }

    private func configureTableViewCell(cell: ActivityTableViewCell, atIndexPath indexPath: NSIndexPath) {
        let activity = activities[indexPath.row]
        cell.updateViewWithActivity(activity, reference: dateButton.date)

        // Add long press gesture recognizer to cell for activating options.
        if cell.gestureRecognizers == nil {
            let longPressGestureRegognizer = UILongPressGestureRecognizer(target: self, action: Selector("handleLongPress:"))
            longPressGestureRegognizer.minimumPressDuration = 0.75;
            cell.gestureRecognizers = [longPressGestureRegognizer]
        }
    }

    @objc private func presentDatePickerViewController() {
        let content = storyboard!.instantiateViewControllerWithIdentifier("DatePicker") as? DatePickerViewController
        if content == nil { return }

        // When date picker "DONE" button selected ...
        content!.setDoneAction { [weak self] vc, date in
            if let sself = self {
                vc.dismiss(completion: nil)
                // ... set the given time for date button and reload the list.
                if sself.dateButton.date != date {
                    sself.dateButton.date = date
                    sself.reloadTableView()
                }
            }
        }

        let modal = EMPartialModalViewController(rootViewController: content!, contentHeight: 240)
        content!.datePickerView?.pickedDate = dateButton.date

        presentViewController(modal, animated: true, completion: nil)
    }

    private func presentActivityViewController(forActivity activity: Activity?) {
        let content = storyboard?.instantiateViewControllerWithIdentifier("Activity") as? ActivityViewController
        if content == nil { return }

        if activity != nil {
            content!.activity = activity!
        }
        let gaActivityDuration: NSTimeInterval = activity?.duration ?? 0
        let gaEventAction = activity == nil ? "Add" : "Edit"
        // When activity view controller "DONE" button selected ...
        content!.setDoneAction { [weak self] vc, activity in
            if let sself = self {
                let ncenter = NotificationCenter.defaultCenter()
                // ... validate picked dates ...
                if activity.start.compare(activity.end) == NSComparisonResult.OrderedDescending {
                    ncenter.fireNotice(Notice(message: .ErrorActivityTimeInvalid))
                    return
                }
                // ... and entered note; ...
                if activity.note.isEmpty {
                    ncenter.fireNotice(Notice(message: .ErrorActivityNoteEmpty))
                    return
                }
                else if count(activity.note) > sself.activityNoteLengthLimit {
                    ncenter.fireNotice(Notice(message: .ErrorActivityNoteTooLong))
                    return
                }
                // ... dismiss the view controller and ...
                vc.dismiss(completion: nil)
                // ... try to save the values.
                DataCenter.defaultCenter().save(activity) { success in
                    var msg: NoticeMessage = NoticeMessage.SuccessItemDidSave
                    if success && activity.hashtags?.count == 0 {
                        msg = NoticeMessage.WarningActivityHashtagEmptyDidSave
                    }
                    else if !success {
                        msg = NoticeMessage.ErrorItemDidNotSave
                    }
                    // Prompt proper message and reload the list.
                    NotificationCenter.defaultCenter().fireNotice(Notice(message: msg))
                    sself.reloadTableView()

                    if success {
                        sself.gaTrackActivityEvent(gaEventAction, value: (activity.duration - gaActivityDuration) / 60)
                    }
                }
            }
        }

        let height: CGFloat = UIScreen.mainScreen().applicationFrame.height > 460 ? 450 : 415
        let modal = EMPartialModalViewController(rootViewController: content!, contentHeight: height)
        modal.contentViewMaxHeight = UIScreen.mainScreen().bounds.height
        
        presentViewController(modal, animated: true, completion: nil)
    }

    @objc private func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state != UIGestureRecognizerState.Began {
            return
        }

        // Define edit button
        let edit = OptionButtonItem(title: "Edit") { [weak self] in
            if let
                sself = self,
                activity = sself.optionsViewController!.userInfo as? Activity
            {
                sself.optionsViewController?.dismiss {
                    sself.presentActivityViewController(forActivity: activity)
                }
            }
        }
        edit.titleLabel.font = ApplicationFontStyle.MainTitle.toFont()
        edit.titleLabel.textColor = UIColor.mineShaftColor()

        // Define delete button
        let delete = OptionButtonItem(title: "Delete", needConfirmation: true) { [weak self] in
            if let
                sself = self,
                activity = sself.optionsViewController!.userInfo as? Activity
            {
                sself.optionsViewController?.dismiss(completion: nil)
                DataCenter.defaultCenter().delete(activity) { success in
                    if success {
                        sself.reloadTableView()
                        sself.gaTrackActivityEvent("Delete", value: activity.duration / -60)
                    }
                }
            }
        }
        delete.titleLabel.font = ApplicationFontStyle.MainTitle.toFont()
        delete.titleLabel.textColor = UIColor.whiteColor()
        delete.backgroundColor = UIColor.jasperColor()

        // Build up option view controller based on created buttons
        let content = OptionsViewController(optionButtons: [delete, edit], cancelCompletion: { [weak self] in
            self?.optionsViewController = nil
        })
        optionsViewController = content

        var frame = content.view.frame
        frame.size.height = content.prefferedHeight
        content.view.frame = frame

        if let
            cell = gestureRecognizer.view as? UITableViewCell,
            indexPath = tableView?.indexPathForCell(cell)
        {
            content.userInfo = activities[indexPath.row]
        }

        let modal = EMPartialModalViewController(rootViewController: content)
        presentViewController(modal, animated: true, completion: nil)
    }

}

// MARK: - HomeChildViewController Methods

extension ActivitiesViewController: HomeChildViewController {

    func addItemActionDidSetOff() {
        presentActivityViewController(forActivity: nil)
    }

}

// MARK: - UITableViewDataSource Methods
// MARK: -

extension ActivitiesViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activities.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: ActivityTableViewCell = tableView.dequeueReusableCellWithIdentifier(tableViewCellId) as! ActivityTableViewCell
        cell.layoutMargins = tableView.layoutMargins

        configureTableViewCell(cell, atIndexPath: indexPath)
        cell.setNeedsUpdateConstraints()
        cell.updateConstraintsIfNeeded()

        return cell
    }

}

// MARK: - ActivitiesViewController Methods
// MARK: -

extension ActivitiesViewController {

    private func gaTrackActivityEvent(action: String, value: Double) {
        let builder = GAIDictionaryBuilder.createEventWithCategory("Activity", action: action, label: nil, value: NSNumber(double: value))
        GAI.sharedInstance().defaultTracker.send(builder.build() as [NSObject: AnyObject])
    }

}